#!/usr/bin/env python
#*- coding: utf-8 -*-

NAME = "Convert any image to Apple II Low-Resolution (Low-Res) graphics, GR"
VERSION = "0.0.6 2015"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://virtualbasic.org"

from PIL import Image, ImageEnhance
from applePalette import loz_GR_colors as apple_GR_color

class ImgToAppleGR:
	"""class ImgToAppleGR version 0.0.6 2015"""
	def __init__(self):
		self.brightness = 70
		self.contrast = 70
		self.imSrc = None
		self.GR_size = (40, 40) # apple screen size
		self.imageType = "Color"
		self.imageRGB_Data = None
		self.imageTest = None
		self.GR_map = None
		self.listing = None
		
	def makeGR_Image(self):
		if self.imSrc:
			self.imageRGB_Data = self.setRGB_ImageSource(im=self.imSrc)
			self.imageTest = self.getImage(data=self.imageRGB_Data)
			self.GR_map = self.setGR_Map(self.imageRGB_Data)
			self.imageData =  self.getColorsArrays(self.GR_map)
			self.listing = self.getBasicListing(data=self.imageData)
		else:
			print "No image source"
			exit()
		
	def setRGB_ImageSource(self, im=None):
		# set brightness
		enhancer = ImageEnhance.Brightness(im)
		factor = self.brightness * (2.0 / 100)
		if factor == 0.0: factor = 1 # zero => reset default val
		im = enhancer.enhance(factor)
		
		# set contrast
		contraster = ImageEnhance.Contrast(im)
		factor = self.contrast * (2.0 / 100)
		if factor == 0.0: factor = 1
		im = contraster.enhance(factor)
		
		# resize image to apple screen sizes
		im = im.resize(self.GR_size)

		# image conversion
		if self.imageType == "Black/White": return self.createBW_ColorMap(im=im)
		if self.imageType == "Color":  		return self.createRGB_ColorMap(im=im)
		
	def getImage(self, data=None):
		chars = ""
		for rgb in data:
			chars += chr(rgb[0])+chr(rgb[1])+chr(rgb[2])
			
		im = Image.frombytes("RGB", self.GR_size, chars)
		im = im.resize((280,160 ))
		return im
		
	def createRGB_ColorMap(self, im=None):
		# GR rgb colors
		index = self.getColorIndex()
		applePalette = []
		for c in index:
			applePalette.extend(c[1:4]) # rgb value
			
		applePalette.extend([0,0,0]*240)
		applePaletteImage = Image.new("P", (1, 1), 0)
		applePaletteImage.putpalette(applePalette)
		colorMap = im.convert("RGB").quantize(palette=applePaletteImage)
		return list(colorMap.convert("RGB").getdata())[0:1600] # get only 40x40
		
	def createBW_ColorMap(self, im=None):
		im = im.convert("1", dither=1)
		return list(im.convert("RGB").getdata())[0:1600]
		
	def setGR_Map(self, data=[]):
		grPixels = []
		grColor = self.get_RGB_Index()
		for e in data:
			grPixels.append(grColor[e])

		return grPixels
		
	def getColorIndex(self):
		index = []
		index.append(["0"]+list(apple_GR_color["black"]))
		index.append(["1"]+list(apple_GR_color["red"]))
		index.append(["2"]+list(apple_GR_color["dark-blue"]))
		index.append(["3"]+list(apple_GR_color["purple"]))
		index.append(["4"]+list(apple_GR_color["dark-green"]))
		index.append(["5"]+list(apple_GR_color["gray"]))
		index.append(["6"]+list(apple_GR_color["medium-blue"]))
		index.append(["7"]+list(apple_GR_color["light-blue"]))
		index.append(["8"]+list(apple_GR_color["brown"]))
		index.append(["9"]+list(apple_GR_color["orange"]))
		index.append(["10"]+list(apple_GR_color["grey"]))
		index.append(["11"]+list(apple_GR_color["pink"]))
		index.append(["12"]+list(apple_GR_color["light-green"]))
		index.append(["13"]+list(apple_GR_color["yellow"]))
		index.append(["14"]+list(apple_GR_color["aqua"]))
		index.append(["15"]+list(apple_GR_color["white"]))
		return index
		
	def getColorsArrays(self, data=[]):
		x = 0;
		colorsArrays = {'0':[],'1':[],'2':[],'3':[],'4':[],'5':[],'6':[],'7':[],'8':[],'9':[],'10':[],'11':[],'12':[],'13':[],'14':[],'15':[]} 		
		for e in data:
			colorsArrays.update({e:colorsArrays[e]+[x%40,x/40]})
			x += 1
			
		return colorsArrays
		
	def getBasicListing(self, data={}):
		XandY = []
		loopColors = []
		for c in sorted(data.keys()):
			if len(data[c]) > 0:
				XandY.extend(data[c])
				loopColors.append([c, len(data[c])/2])# one loop = 2 values (x,y)

		# -------------------- basic data
		basicData = []	
		textWrap = []	
		for e in XandY:
			if len(textWrap) == 39:
				textWrap.append(str(e))
				basicData.append("DATA "+",".join(textWrap))
				textWrap = []  
			else:
				textWrap.append(str(e))
			
		if len(textWrap) > 0:
			basicData.append("DATA "+",".join(textWrap))
		# -------------------- end data
		
		basicCode = []
		# basicCode.append("HOME:GR:POKE 49234,0:CALL -1998") # full screen
		basicCode.append("HOME:GR")
		for e in loopColors:
			basicCode.append("COLOR="+str(e[0])+":FOR I=1 TO "+str(e[1])+":READ X:READ Y:PLOT X,Y:NEXT" )
	
		# basicCode.append("RESTORE:GOTO 10:REM INFINITE LOOP")
		# basicCode.append("GET A$")
		# basicCode.append("TEXT:HOME")
		basicCode.append("END")
		basicCode.extend(basicData)
		
		# add line numbers
		lines = []
		num = 0
		for line in basicCode:
			num += 10
			lines.append(str(num)+" "+line)

		listing = "\n".join(lines)
		return listing
		
	def get_RGB_Index(self):
		index = self.getColorIndex()
		RGB_Index = {}
		for c in index:
			RGB_Index.update({tuple(c[1:4]):c[0]})
			
		return RGB_Index
		
	def get_GR_Index(self):
		index = self.getColorIndex()
		GR_Index = {}
		for c in index:
			GR_Index.update({c[0]:c[1:4]})
			
		return GR_Index
		
	def saveImg(self, im=None, fileName="test.png", r=(0,0)):
		if r != (0,0):
			im = im.resize(r)
		try:
			im.save(fileName,"PNG")
			return 1
		except:
			print "can't save",fileName
			return 0
			
if __name__ == "__main__":
	print NAME
	print ImgToAppleGR.__doc__
	print "\n".join([AUTHOR,COPYRIGHT,URL])