#!/usr/bin/env python
#*- coding: utf-8 -*-

from os import path, getcwd, mkdir, name as thisSystem
from copy import copy
import re
from math import floor, ceil
from random import randint, choice
from time import localtime, strftime, sleep, time
from ast import literal_eval
from subprocess import call, Popen
import pickle

# tk libraries
from Tkinter import *
import tkFileDialog as tkDia
import tkMessageBox as tkMsg
import tkColorChooser
import tkFont
	
try: # need PIL library to work
	from PIL import Image, ImageTk, ImageDraw, ImageOps
except:
	tkMsg.showerror("Error", "You must install PIL Python Image Library\nhttps://pypi.python.org/pypi/Pillow")
	exit()

# apple image lib	
from imgToAppleHGR import ImgToAppleHGR, apple_HGR_color
from imgToAppleGR import ImgToAppleGR

# external programs
ffmpeg = ""
gimp = ""
if thisSystem == "posix": # linux system
	ffmpeg = "/usr/bin/ffmpeg" # set your own path to ffmpeg
	gimp = "/usr/bin/gimp" # set your own path to gimp
	appleWin = ""
if thisSystem == "nt": # windows system
	ffmpeg = "C:/ffmpeg/bin/ffmpeg.exe"
	gimp = "C:/Program Files/GIMP 2/bin/gimp-2.8.exe"
	appleWin = "C:/appleWin/Applewin.exe"
	
NAME = "Apple Image Editor Graphical User Interface"
VERSION = "0.1.3 2018"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://hyperficiel.com"

class AppleImageEditor:
	"""Class AppleImageEditor version 0.1.2 2016"""
	def __init__(self):
		# roots dialogs
		self.restoreVars()
		
		# thumbnails, images
		self.originalFileName = ""
		self.originalImage = None 		# instance of original image (by open, or remplace, or buffer)
		self.appleScreenImage = None 	# instance of the result image (apple screen)
		self.pixelMapImage = None 		# instance of pixel map
		self.tempImgSourceData = {}
		self.pixelMap = []
		self.appleBinColorMap = []
		self.MSB_Map = [] 				# not in use
		
		# images buffers
		self.buffers = {}
		self.bufferFileNames = []
		
		# params buffer
		self.datParams = {}
		
		# undo field hexadecimal
		self.undoHexa = []
		
		# imgToAppleHGR image instance
		self.appleImage = None
		# imgToAppleGR image instance
		self.imageGR = None
		self.originalGR_Image = None
		
		# colors, sizes
		self.selectColor = [0,0,0]
		self.defaultBgAlt = "#FFFFFF"
		self.A_Colors = apple_HGR_color
		self.createAppleColors()
		self.appleScr = (280, 192)
		self.zoomScr = (1120, 768)
		
		# bitwize vars
		self.bits = [0]*7
		self.bitButton = [None]*7
		self.bitWise = "^0000000"
		self.bitWiseOperator = "^"
		
		# others
		self.appleScreenThumbnailX2 = 0
		self.GR_FinalThumbnailX2 = 0
		self.clipboardDataType = "monitor listing"
		
		# window vars and draw interface
		self.tkRoot = Tk()
		self.defaultBgColor = self.tkRoot.cget("bg")
		self.tkRoot.title("Apple Image Editor")
		self.fontSize = 10
		self.monoFontSize = 9
		self.fontFamily = "Arial"
		self.customFont = tkFont.Font(family=self.fontFamily, size=self.fontSize)
		self.customFontMini = tkFont.Font(family=self.fontFamily, size=self.fontSize-2)
		self.customFontBold = tkFont.Font(family=self.fontFamily, size=self.fontSize, weight="bold")
		self.customMonoFont = tkFont.Font(family="courier", size=self.monoFontSize)
		
		# win position
		self.winZoomPos = ""
		self.winBitWisePos = ""
		self.winConverterPos = ""
		self.winFindPos = ""
		self.winInspectVarsPos = ""
		self.winGR_Pos = ""
		self.winAnimationPos = ""
		
		# find and replacevars	
		self.searchText = StringVar()
		self.replaceText = StringVar()
		self.nextFind = 1.0
		self.findTextPos = None
		self.selectedInField = ""
		self.regexStatus = IntVar()
		self.regexStatus.set(0)
		
		# convert vars
		self.convertDecimalVal = StringVar()
		self.convertHexVal = StringVar()
		self.convertBinaryVal = StringVar()
		
		# draw interface
		self.drawInterface()
		# set to local default example if exist
		self.loadImageDefault(fileName="bacall.jpg")
		# load interface
		self.tkRoot.mainloop()
	
	# -------------------------------- GUI tools
	def quitInterface(self, event=None):
		self.storeVars()
		self.tkRoot.destroy()
		
	def about(self):
		msg = "\n".join([AUTHOR,URL])
		tkMsg.showinfo("Author", msg)
		
	def appleImageEditorInfos(self):
		msg = "\n".join(["Apple Image Editor Graphical User Interface", AppleImageEditor.__doc__, ImgToAppleHGR.__doc__, ImgToAppleGR.__doc__, COPYRIGHT])
		tkMsg.showinfo("Apple Image Editor infos", msg)
		
	def helpHexadecimalRaw(self, event=None):
		msg = """Raw hexadecimal code correspond to
Top to bottom 192 lines of
40 bytes for each line in the final apple 
screen picture
If field content not reach 192 X 40 bytes then
the field is filled with colored bytes,
colored bytes are always added to the bottom

parameters in left white zone are disabled
when you edit the hexadecimal field
"""
		tkMsg.showinfo("Help hexadecimal raw", msg)
		
	def helpHexadecimal(self, event=None):
		msg = """Hexadecimal code correspond to
Apple image binary data in Apple II memory from $2000 to $3FFF
Test the hexa code with this basic script in your Apple Emulator
10 HGR
20 VTAB(22)
30 CALL -151
40 END
RUN
and paste the hex code in monitor...
"""
		tkMsg.showinfo("Help hexadecimal", msg)
	
	def helpResetButton(self, event=None):
		msg = """Press reset button to reset image to original
all parameters return to default
all hexadecimal modifications are lost
"Image Source" is now converted from original image
"""
		tkMsg.showinfo("Help reset button", msg)
		
	def helpRefreshButton(self, event=None):
		msg = """Press resfresh button to apply recent changes
to the result image because some buttons 
not refresh automatically the image
"""
		tkMsg.showinfo("Help refresh button", msg)
		
	def helpOpenBinHexa(self, event=None):
		msg = """When you open binary files or hexadecimal files
there is no image source then 
parameters in left white zone are disabled
"""
		tkMsg.showinfo("Help BinHexa", msg)
		
	def helpAddNewBuffer(self, event=None):
		msg = """Adding Apple Screen result to buffers
set new image from data like opening a Apple binary file
then parameters in left white zone are disabled
"""
		tkMsg.showinfo("Help New Buffer", msg)
	
	def helpSubject(self, event=None):
		msg = """
"""
		tkMsg.showinfo("Help ", msg)
	
	# stores vars
	def storeVars(self):
		with open("vars", "w") as f:
			vars = {"openDiaRoot":self.openDiaRoot, "saveDiaRoot":self.saveDiaRoot,"openDiaParamsRoot":self.openDiaParamsRoot, "saveDiaParamsRoot":self.saveDiaParamsRoot}
			pickle.dump(vars, f)

	def restoreVars(self):
		try:
			with open("vars") as f:
				params = pickle.load(f)
				self.openDiaRoot = params["openDiaRoot"]
				self.saveDiaRoot = params["saveDiaRoot"]
				self.openDiaParamsRoot = params["openDiaParamsRoot"]
				self.saveDiaParamsRoot = params["saveDiaParamsRoot"]
		except:
			self.saveDiaRoot = getcwd()
			self.openDiaRoot = getcwd()
			self.openDiaParamsRoot = getcwd()
			self.saveDiaParamsRoot = getcwd()
	
	def testPattern(self):
		bytColors = [self.white*7, self.black*7, self.green*7, self.violet*7, self.orange*7, self.blue*7, self.black*7, self.white*7]
		
		chars = ""
		chars += ((bytColors[0]+bytColors[1])*100)  + ((bytColors[1]+bytColors[0])*100) # black/white stripes
		
		for z in xrange(0,172): # colors colors columns
			for bytColor in bytColors:
				chars += bytColor*5
				
		chars += ((bytColors[0]+bytColors[1])*100) + ((bytColors[1]+bytColors[0])*100) # black/white stripes
					 
		self.originalImage = Image.frombytes("RGB", self.appleScr, chars)
		
		# lines, rectangles, circle
		draw = ImageDraw.Draw(self.originalImage)
		draw.rectangle((70, 49, 209, 143), fill=self.A_Colors["black"]) # black rectangle
		
		for i in xrange(70,211,3): # white verticales lines
			draw.line((i, 50, i, 142), fill=self.A_Colors["white"])
			
		draw.line((0, 0, 280, 192), fill=self.A_Colors["green"]) 	# green diagonal
		draw.line((0, 192, 280, 0), fill=self.A_Colors["violet"])	# violet diagonal
		draw.line((139, 0, 139, 192), fill=self.A_Colors["blue"])	# blue vertical
		draw.line((0, 96, 280, 96), fill=self.A_Colors["orange"])	# orange horizontal
		draw.ellipse((120, 76, 160, 116), fill=self.A_Colors["white"]) # white circle
		del draw
		
		self.originalFileName = "Example-Test-Pattern"
		self.tkRoot.title("Apple Image Editor \> "+self.originalFileName)
		# clear image from others data sources
		self.tempImgSourceData = {}
		# create Apple Image
		self.createNewAppleImage()
		# create thumbails
		self.createThumbnails()
		# show hide text by default
		self.imageMarkContent.set(self.appleImage.hideText)
		# get hex to field
		self.doHexadecimal()
		# trick for fast settings
		self.imageType.set("HGR")
		# self.brightness.set(50)
		# self.contrast.set(50)
		# add to buffer
		bufferData = [0, self.originalImage, self.appleImage.appleBinData, self.appleImage.pixelMap, self.appleImage.appleBinColorMap, self.getEditorParams()]
		self.addFileToBuffers(file=self.originalFileName, data=bufferData)
		self.refreshAppleImage()
				
	def loadImageDefault(self, fileName=None):
		if path.isfile(fileName):
			try:
				self.originalImage = Image.open(fileName).resize(self.appleScr)
				self.originalFileName = fileName
				self.tkRoot.title("Apple Image Editor \> "+self.originalFileName)
				
				# clear image from others data sources
				self.tempImgSourceData = {}
				# create Apple Image
				self.createNewAppleImage()
				# create thumbails
				self.createThumbnails()
				# show hide text by default
				self.imageMarkContent.set(self.appleImage.hideText)
				# get hex to field
				self.doHexadecimal()
				# trick for fast settings
				self.imageType.set("HGR")
				self.brightness.set(70)
				self.contrast.set(70)
				# add to buffer
				bufferData = [0, self.originalImage, self.appleImage.appleBinData, self.appleImage.pixelMap, self.appleImage.appleBinColorMap, self.getEditorParams()]
				self.addFileToBuffers(file=self.originalFileName, data=bufferData)
				self.refreshAppleImage()
				return 1
			except:
				self.testPattern()
		else:
			self.testPattern()
	
	def addFileToBuffers(self, file="", data=None):
		if file not in self.buffers.keys():# avoid duplication
			self.mBuffers.add_command(label=file, command=lambda: self.loadFromBuffer(file=file))
			self.buffers.update({file:data})
			self.bufferFileNames.append(file)
			return 1
		else:
			return 0
		
	def deleteBufferEntry(self, event=None):
		if len(self.bufferFileNames):# while items
			fileName = self.originalFileName
			# delete
			self.mBuffers.delete(fileName)# delete item menu
			del self.buffers[fileName]# delete dict entry
			self.bufferFileNames.remove(fileName) # delete name
			try:
				# reload first buffer
				if self.bufferFileNames: self.loadFromBuffer(file=self.bufferFileNames[0])
			except:
				pass
				
			return 1
		else:
			return 0
				
	def loadFromBuffer(self, file=file):
		try:
			# first store editor previous workspace params
			data = self.buffers[self.originalFileName] 
			data.pop()
			data.append(self.getEditorParams())
			self.buffers.update({self.originalFileName:data})
		except:
			pass
			# print "buffer", self.originalFileName, "doesn't exist anymore"

		# update editor
		self.originalFileName = file
		self.tkRoot.title("Apple Image Editor \> "+self.originalFileName)
		
		# set tempImgSourceData with buffer data
		# 0 -> source boolean
		# 1 -> self.originalImage
		# 2 -> self.appleImage.appleBinData
		# 3 -> self.appleImage.pixelMap
		# 4 -> self.appleImage.appleBinColorMap
		# 5 -> self.getEditorParams()
		bufferData = self.buffers[file]
		if bufferData[0]: 		# from buffer
			self.originalImage = bufferData[1]
			self.tempImgSourceData.update({ 
				"original":		copy(bufferData[1]),
				"appleBinData":	copy(bufferData[2]),
				"pixel_map":	copy(bufferData[3]),
				"color_map":	copy(bufferData[4])
			})
			self.createNewAppleImageData()
			self.restoreEditorParams(params=bufferData[5])
			self.refreshAppleImage()
		else: 					# from image
			self.tempImgSourceData = {}
			self.originalImage = bufferData[1]
			self.createNewAppleImage()
			self.restoreEditorParams(params=bufferData[5])
			self.refreshAppleImage()
		
		# create thumbails
		self.createThumbnails()
		# show hide text by default
		self.imageMarkContent.set(self.appleImage.hideText)
		# get hex to field
		self.doHexadecimal()
	
	# -------------------------------- dat params	
	def openParams(self, event=None):
		fileName = tkDia.askopenfilename(parent=self.tkRoot, title="Open Dat File", initialdir=self.openDiaParamsRoot, filetypes=[("Data File","*.dat"), ("all files", ".*")])
		if fileName:
			self.openDiaParamsRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			data = self.openDatFile(fileName=fileName)
			try:
				self.restoreEditorParams(params=literal_eval(data)[2], hideText=0)
				self.addToParamsBuffer(file=fileName, data=literal_eval(data)[2])
				paramEntry = re.compile(r" \[.*\]")
				title = paramEntry.sub("", self.tkRoot.title())
				self.tkRoot.title(title+" [with params file : "+fileName+"]")
				self.refreshAppleImage()
			except:
				self.winCancel("can't read params")
				return 0
		else:
			self.winCancel("open dat file cancelled")
			return 0
		return 1
		
	def addToParamsBuffer(self, file="", data=None):
		if file not in self.datParams.keys():# avoid duplication
			self.mParamsBuffers.add_command(label=file, command=lambda: self.loadParams(file=file))
			self.datParams.update({file:data})
			return 1
		else:
			return 0
			
	def deleteParamsBufferEntry(self, event=None):
		paramEntry = re.compile("( \[with params file : .*\])")
		datFile = paramEntry.search(self.tkRoot.title())
		if datFile:
			file = datFile.group(1)
			title = self.tkRoot.title()
			title = title.replace(file,"")
			self.tkRoot.title(title)
			del self.datParams[file[21:-1]]
			self.mParamsBuffers.delete(file[21:-1])# delete item menu
		else:
			self.winError("no parameter dat file in use")
			
	def loadParams(self, file=file):
		paramEntry = re.compile(r" \[.*\]")
		title = paramEntry.sub("", self.tkRoot.title())
		self.tkRoot.title(title+" [with params file : "+file+"]")
		data = self.datParams[file] 
		self.restoreEditorParams(params=data, hideText=0)
		self.refreshAppleImage()
		
	def cleanTitleParams(self):
		paramEntry = re.compile(r" \[.*\]")
		title = paramEntry.sub("", self.tkRoot.title())
		self.tkRoot.title(title)
		
	# -------------------------------- picture	
	def openImageSource(self, event=None):
		fileName = tkDia.askopenfilename(parent=self.tkRoot, title="Open Image File", initialdir=self.openDiaRoot, filetypes=[("Image",("*.png","*.jpg","*.jpeg","*.gif")), ("all files", ".*")])
		if fileName:
			self.openDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			# set original image
			try:
				self.originalImage = Image.open(fileName).resize(self.appleScr)
				# self.originalImage = ImageOps.posterize(self.originalImage, 3)
			except:
				self.winErrorFile("can't open "+fileName)
				return 0
				
			# set image values with new image
			self.originalFileName = fileName
			self.tkRoot.title("Apple Image Editor \> "+self.originalFileName)
			
			# set to None image from data sources
			self.tempImgSourceData = {}
			# create Apple Image
			self.createNewAppleImage()
			# create thumbails
			self.createThumbnails()
			# show hide text by default
			self.imageMarkContent.set(self.appleImage.hideText)
			# get hex to field
			self.doHexadecimal()
			# better image with a bit brightness and contrast in HGR
			self.imageType.set("HGR")
			self.brightness.set(70)
			self.contrast.set(70)
			# add to bufferData : originalImage, appleBinData, pixelMap, appleBinColorMap, imgVars
			bufferData = [0, self.originalImage, self.appleImage.appleBinData, self.appleImage.pixelMap, self.appleImage.appleBinColorMap, self.getEditorParams()]
			self.addFileToBuffers(file=self.originalFileName, data=bufferData)
			self.refreshAppleImage()
		else:
			self.winCancel("open image source cancelled")
			return 0
		return 1
		
	def createNewAppleImage(self):
		# delete previous ImgToAppleHGR object
		try:
			del self.appleImage
		except:
			pass
			
		# create Apple Image with ImgToAppleHGR
		self.appleImage = ImgToAppleHGR() # create appleImage object
		self.appleImage.imageType = self.imageType.get()
		self.appleImage.imSrc = self.originalImage
		self.appleImage.makeAppleImage()
		
	def createThumbnails(self):
		# original image -> self.originalImage
		self.canOriginalImage = ImageTk.PhotoImage(self.originalImage)
		# always need an image object ex: self.xxx in self.canvasXxx, why ?
		self.canOriginal.create_image(3,3, anchor= NW, image=self.canOriginalImage)
		
		# pixel map image -> from imgToAppleHGR pixelMap
		self.pixelMapImage = Image.frombytes("L", self.appleScr, "".join(map(chr, self.appleImage.pixelMap)))
		self.canPixelMapImage = ImageTk.PhotoImage(self.pixelMapImage)
		self.canPixelMap.create_image(3,3, anchor= NW, image=self.canPixelMapImage)
		
		# result image -> from imgToAppleHGR appleBinData or pixelMap
		self.appleScreenImage = self.getAppleScreenImage()
		# adjust size of the thumbnail
		self.appleScreenImageThumb = self.appleScreenImage
		if self.appleScreenThumbnailX2:
			self.appleScreenImageThumb = self.appleScreenImageThumb.resize((self.appleScr[0]*2,self.appleScr[1]*2))
			
		self.canAppleScreenImage = ImageTk.PhotoImage(self.appleScreenImageThumb)
		self.canAppleScreen.create_image(3,3, anchor=NW, image=self.canAppleScreenImage)
		
	def appleScreenImageBig(self, event=None):
		if self.appleScreenThumbnailX2 == 1:
			self.appleScreenThumbnailX2 = 0
			self.canAppleScreen.configure(width=self.appleScr[0], height=self.appleScr[1])
		else:
			self.appleScreenThumbnailX2 = 1
			self.canAppleScreen.configure(width=self.appleScr[0]*2, height=self.appleScr[1]*2)
			
		self.createThumbnails()

	# -------------------------------- binary		
	def openAppleBinaryFile(self, event=None):
		fileName = tkDia.askopenfilename(parent=self.tkRoot, title="Open Binary Image File", initialdir=self.openDiaRoot, filetypes=[("Apple Image bin","*.bin"), ("all files", ".*")])
		if fileName:
			self.openDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			# read binary file
			bytes = []
			try:
				f = open(fileName, "rb")
				for i in xrange(0,8192,1024):
					bytes.extend(map(ord, f.read(1024)))
					
				f.close()
				
				# extract hidetext
				self.imageMarkContent.set(self.extractHideTextFromFile(fileName))
				
				# change order of bytes to linear
				bytes = self.appleBinToLinearOrder(bytes=bytes)
			except:
				self.winErrorFile("can't open or read "+fileName+" or get hide text error")
				return 0
			
			data = self.openDatFile(fileName=fileName)
		else:
			self.winCancel("open bin cancelled")
			return 0
			
		# create Apple Image data
		self.createAppleImageSourceFromData(bytes=bytes, data=data)
		
		# set image values with new image
		self.originalFileName = fileName
		self.tkRoot.title("Apple Image Editor \> "+self.originalFileName)
		# create Apple Image with ImgToAppleHGR but with no imSrc
		self.createNewAppleImageData()
		# create thumbails
		self.createThumbnails()
		# get hex to field
		self.doHexadecimal()
		# add file to buffer
		bufferData = [1, self.originalImage, self.appleImage.appleBinData, self.appleImage.pixelMap, self.appleImage.appleBinColorMap, self.getEditorParams()]
		self.addFileToBuffers(file=self.originalFileName, data=bufferData)
		return 1
	
	def createAppleImageSourceFromData(self, bytes=bytes, data=None):
		if data: # from dat file
			pixelMap = literal_eval(data)[0]
			colorMap = literal_eval(data)[1]
		else: # if not : set pixel map from bytes and set colormap to empty array
			pixelMap = self.byteToSevenPixels(bytes=bytes)
			colorMap = []
		
		newOriginal = self.getAppleScreenInColor(data=bytes)
		# very important ! set tempImgSourceData to replace appleImage.imSrc and stop to compute from pict when makeAppleImage
		self.tempImgSourceData = {
			"appleBinData":	bytes,
			"original":		newOriginal,
			"pixel_map":	pixelMap,
			"color_map":	colorMap
		}
		
		# set original image
		self.originalImage = newOriginal
		
	def setAppleImageSource(self):
		self.appleImage.imSrc 				= None
		self.appleImage.pixelMap 			= copy(self.tempImgSourceData["pixel_map"])
		self.appleImage.appleBinData 		= copy(self.tempImgSourceData["appleBinData"])
		self.appleImage.appleBinColorMap 	= copy(self.tempImgSourceData["color_map"])
		
	def createNewAppleImageData(self):
		# delete previous ImgToAppleHGR object
		try:
			del self.appleImage
		except:
			pass
			
		self.appleImage = ImgToAppleHGR() # create appleImage object
		# use copy to have independents arrays
		self.appleImage.pixelMap 			= copy(self.tempImgSourceData["pixel_map"])
		self.appleImage.appleBinData 		= copy(self.tempImgSourceData["appleBinData"])
		self.appleImage.appleBinColorMap 	= copy(self.tempImgSourceData["color_map"])		
		self.appleImage.makeAppleImage()
	
	def appleBinToLinearOrder(self, bytes=bytes):
		# order of 192 lines in apple bin pict, only 7680 bytes
		appleBinBytesLines = [
			[0, 	 40],	[1024, 1064],	[2048, 2088],	[3072, 3112],	[4096, 4136],	[5120, 5160],	[6144, 6184],	[7168, 7208],	[128, 	168],	[1152, 1192],	
			[2176, 2216],	[3200, 3240],	[4224, 4264],	[5248, 5288],	[6272, 6312],	[7296, 7336],	[256, 	296],	[1280, 1320],	[2304, 2344],	[3328, 3368],	
			[4352, 4392],	[5376, 5416],	[6400, 6440],	[7424, 7464],	[384, 	424],	[1408, 1448],	[2432, 2472],	[3456, 3496],	[4480, 4520],	[5504, 5544],	
			[6528, 6568],	[7552, 7592],	[512, 	552],	[1536, 1576],	[2560, 2600],	[3584, 3624],	[4608, 4648],	[5632, 5672],	[6656, 6696],	[7680, 7720],	
			[640, 	680],	[1664, 1704],	[2688, 2728],	[3712, 3752],	[4736, 4776],	[5760, 5800],	[6784, 6824],	[7808, 7848],	[768, 	808],	[1792, 1832],	
			[2816, 2856],	[3840, 3880],	[4864, 4904],	[5888, 5928],	[6912, 6952],	[7936, 7976],	[896, 	936],	[1920, 1960],	[2944, 2984],	[3968, 4008],	
			[4992, 5032],	[6016, 6056],	[7040, 7080],	[8064, 8104],	[40, 	 80],	[1064, 1104],	[2088, 2128],	[3112, 3152],	[4136, 4176],	[5160, 5200],	
			[6184, 6224],	[7208, 7248],	[168, 	208],	[1192, 1232],	[2216, 2256],	[3240, 3280],	[4264, 4304],	[5288, 5328],	[6312, 6352],	[7336, 7376],	
			[296,	336],	[1320, 1360],	[2344, 2384],	[3368, 3408],	[4392, 4432],	[5416, 5456],	[6440, 6480],	[7464, 7504],	[424, 	464],	[1448, 1488],	
			[2472, 2512],	[3496, 3536],	[4520, 4560],	[5544, 5584],	[6568, 6608],	[7592, 7632],	[552, 	592],	[1576, 1616],	[2600, 2640],	[3624, 3664],	
			[4648, 4688],	[5672, 5712],	[6696, 6736],	[7720, 7760],	[680, 	720],	[1704, 1744],	[2728, 2768],	[3752, 3792],	[4776, 4816],	[5800, 5840],	
			[6824, 6864],	[7848, 7888],	[808, 	848],	[1832, 1872],	[2856, 2896],	[3880, 3920],	[4904, 4944],	[5928, 5968],	[6952, 6992],	[7976, 8016],	
			[936, 	976],	[1960, 2000],	[2984, 3024],	[4008, 4048],	[5032, 5072],	[6056, 6096],	[7080, 7120],	[8104, 8144],	[80, 	120],	[1104, 1144],	
			[2128, 2168],	[3152, 3192],	[4176, 4216],	[5200, 5240],	[6224, 6264],	[7248, 7288],	[208,	248],	[1232, 1272],	[2256, 2296],	[3280, 3320],	
			[4304, 4344],	[5328, 5368],	[6352, 6392],	[7376, 7416],	[336, 	376],	[1360, 1400],	[2384, 2424],	[3408, 3448],	[4432, 4472],	[5456, 5496],	
			[6480, 6520],	[7504, 7544],	[464, 	504],	[1488, 1528],	[2512, 2552],	[3536, 3576],	[4560, 4600],	[5584, 5624],	[6608, 6648],	[7632, 7672],	
			[592, 	632],	[1616, 1656],	[2640, 2680],	[3664, 3704],	[4688, 4728],	[5712, 5752],	[6736, 6776],	[7760, 7800],	[720, 	760],	[1744, 1784],	
			[2768, 2808],	[3792, 3832],	[4816, 4856],	[5840, 5880],	[6864, 6904],	[7888, 7928],	[848, 	888],	[1872, 1912],	[2896, 2936],	[3920, 3960],	
			[4944, 4984],	[5968, 6008],	[6992, 7032],	[8016, 8056],	[976,  1016],	[2000, 2040],	[3024, 3064],	[4048, 4088],	[5072, 5112],	[6096, 6136],	
			[7120, 7160],	[8144, 8184]
		]
		linearOrder = []
		for x, y in appleBinBytesLines:
			linearOrder.extend(bytes[x:y])
		
		return linearOrder	
		
	def appleBinToImageBytes(self, bytes=bytes):
		# change bytes order and convert to pixels
		bytes = self.appleBinToLinearOrder(bytes=bytes)
		pixels = self.byteToSevenPixels(bytes=bytes)
		
	def byteToSevenPixels(self, bytes=[]):
		# every byte represent seven pixels
		pixels = []
		for byte in bytes:
			byteStr = str(bin(byte))[2:].zfill(8)
			byteStr = byteStr[1:] # pop Most significant bit
			byteStr = byteStr[::-1] # inverse bits order
			for i in xrange(0,7):
				if byteStr[i] == "1":
					pixels.append(255) # use "L" image mode
				else:
					pixels.append(0)
		return pixels
	
	def get_MSB_MapFromBytes(self, data=[]):
		# create a map of MSB bits
		MSB_map = []
		for e in data:
			if e > 127:
				MSB_map.append(1)
			else:
				MSB_map.append(0)
		return MSB_map
		
	# -------------------------------- hexadecimal 
	def openHexaFileListing(self, event=None):
		fileName = tkDia.askopenfilename(parent=self.tkRoot, title="Open Hexadecimal Text File", initialdir=self.openDiaRoot, filetypes=[("Apple Hexadecimal","*.txt"), ("all files", ".*")])
		if fileName:
			self.openDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			try:
				f = open(fileName, "r") # open hex code listing
				lines = f.readlines()
				f.close()
			except:
				self.winErrorFile("can't open or read "+fileName)
				return 0
				
		else:
			self.winCancel("open hexa listing cancelled")
			return 0
		
		hexData = []
		for line in lines:# read hex code listing
			if len(line) > 5:
				line = line[5:].rstrip() # rip offset adress
				hexData.extend(line.split()) # add bytes in hex 
		
		bytes = []
		try:
			bytes = map(lambda x: int(x, 16), hexData) # convert hex in decimal
			
			# extract hidetext before change order
			self.imageMarkContent.set(self.extractHideTextFromListing(bytes=bytes))
			
			# change order of bytes to linear
			bytes = self.appleBinToLinearOrder(bytes=bytes)
		except:
			self.winErrorFile("can't read hexadecimal from "+fileName)
			return 0
		
		# create Apple Image data
		data = self.openDatFile(fileName=fileName)# open dat file if exist
		self.createAppleImageSourceFromData(bytes=bytes, data=data)
		
		# set image values with new image
		self.originalFileName = fileName
		self.tkRoot.title("Apple Image Editor \> "+self.originalFileName)
		# create Apple Image with ImgToAppleHGR but with no imSrc
		self.createNewAppleImageData()
		# create thumbails
		self.createThumbnails()
		# get hex to field
		self.doHexadecimal()
		# add to buffer
		bufferData = [1, self.originalImage, self.appleImage.appleBinData, self.appleImage.pixelMap, self.appleImage.appleBinColorMap, self.getEditorParams()]
		self.addFileToBuffers(file=self.originalFileName, data=bufferData)
		return 1
	
	def doRawHex(self, event=None):
		if self.rawHex.get() == 1:
			self.rawHex.set(0)
		else:
			self.rawHex.set(1)
			
		self.doHexadecimal()
		return "break"
		
	def doHexadecimal(self):
		hexaCode = ""
		if self.rawHex.get():
			# get the hex code from appleImage object var appleBinData because it's the 7680 raw bytes array
			hexArray = [hex(x)[2:].upper() for x in self.appleImage.appleBinData]
			for i in xrange(0,len(self.appleImage.appleBinData),40):
				hexaCode += " ".join([x.zfill(2) for x in hexArray[i:i+40]])+"\n"
		else:		
			# get hex from appleImage
			hexaCode = self.appleImage.getHexaCode()
			
		# and copy to clipboard
		self.fieldCodeHexa.delete(0.1, END)
		self.fieldCodeHexa.insert(INSERT, hexaCode)
		self.copyHexaToClipboard()
		
	def getImageDataFromHexadecimal(self, event=None):
		hexCode = self.fieldCodeHexa.get(0.1, END)
		lines = hexCode.split("\n")
		hex = []
			
		# raw data mode, freely manipulate data from pict
		if self.rawHex.get():
			for line in lines:
				line = line.rstrip()
				hex.extend(line.split())
				
			try:
				bytes = map(lambda x: int(x, 16), hex) # convert to bytes
			except:
				self.winError("can't read hexadecimal from field")
				return 0
			
			# if field not contains enough data fill with bytes
			bytes = self.addHexFillingBytes(bytes=bytes, max=7680)
		else:
			for line in lines:
				if len(line) > 5: # check start offset adress of line
					line = line[5:].rstrip()
					hex.extend(line.split())
					
			try:
				bytes = map(lambda x: int(x, 16), hex) # convert to bytes
			except:
				self.winError("can't read hexadecimal from field")
				return 0
		
			bytes = self.addHexFillingBytes(bytes=bytes, max=8192) # full appleBin image with hide text
			bytes = self.appleBinToLinearOrder(bytes=bytes) # only usefull bytes (7680) returned
			
		pixelMap = self.byteToSevenPixels(bytes)
		return (bytes, pixelMap)
		
	def addHexFillingBytes(self, bytes=[], max=7680):
		if len(bytes) < max: # only if len bytes < max
			fillingBytes = [];maskEvenOdd = 43;msb = 0
			if self.rawHexFillColor.get() == "Random":
				fillingByte = [randint(0,127)]
			else:
				fillingByte = [self.rawHexFillColorValues[self.rawHexFillColor.get()]]
			
			if fillingByte[0] > 127 and fillingByte != [255]:# add msb
				msb = 128
				fillingByte[0] = fillingByte[0] - 128 # substract now to add after
			
			# full colored bytes and seven pixels
			# 	"Black":		0	= 00 00 00 00 			 			
			#	"White":		255 = 11 11 11 11
			#	"Violet/Blue":	42 	= 00 10 10 10
			#	"Green/Orange":	85	= 01 01 01 01						  
			# 	maskEvenOdd : 	43  = 00 10 10 11		
			
			if fillingByte == [85]: maskEvenOdd = -43 # inverse when green/orange

			evenOrOdd = 1
			if len(bytes) % 2 == 0: evenOrOdd = 0 # check last byte if even or odd
				
			diff = max - len(bytes)
			fillingBytes = fillingByte*diff

			for byte in fillingBytes:
				if evenOrOdd % 2 == 0 and byte != 0 and byte != 255:
					byte += maskEvenOdd
				elif self.rawHexFillColor.get() == "Random X": # every byte random
					byte = randint(0,255)
				elif self.rawHexFillColor.get() == "Random X2": # every byte random
					byte = randint(0,85)	
				
				# add msb
				byte = byte + msb
						
				bytes.append(byte)
				evenOrOdd += 1
			
			if len(bytes) > max: # prevent bytes overflow
				bytes = bytes[:max]
		
		return bytes
	
	def copyHexaToClipboard(self, event=None, data=None, dataType="monitor listing"):
		if data == None:
			dataType = "monitor listing"
			data = self.fieldCodeHexa.get(0.1, END)
			
		self.tkRoot.clipboard_clear()
		self.tkRoot.clipboard_append(data)
		self.clipboardDataType = dataType
		# print "code copied to clipboard, style:",dataType
		
	# -------------------------------- open data file if exist
	def openDatFile(self, fileName=None):	
		# open dat file if exist
		data = None
		if path.isfile(fileName[:-4]+".dat"):
			try: 
				f = open(fileName[:-4]+".dat", "r")
				data = f.read()
				f.close()
			except:
				self.winErrorFile("can't open or read "+fileName[:-4]+".dat")
				return 0
		return data
		
	# -------------------------------- new buffer / reset / refresh
	def newImageSource(self, event=None):
		if self.appleImage:
			# first store editor previous workspace params
			data = self.buffers[self.originalFileName] 
			data.pop()
			data.append(self.getEditorParams())
			self.buffers.update({self.originalFileName:data})
		
			# remplace image original by data source image
			self.originalImage = self.appleScreenImage
			self.appleImage.imSrc = None
			self.tempImgSourceData.update({ 
				"original":		copy(self.originalImage),
				"appleBinData":	copy(self.appleImage.appleBinData),
				"pixel_map":	copy(self.appleImage.pixelMap),
				"color_map":	copy(self.appleImage.appleBinColorMap)
			})
			
			# reset params
			self.resetImageParams()
			self.resetImageParamsExtras()
			self.refreshAppleImage()
			
			# add to buffer with name and random num
			self.originalFileName = "buffer-"+str(randint(1000000,9999999))
			bufferData = [1, self.originalImage, self.appleImage.appleBinData, self.appleImage.pixelMap, self.appleImage.appleBinColorMap, self.getEditorParams()]
			self.addFileToBuffers(file=self.originalFileName, data=bufferData)
			self.tkRoot.title("Apple Image Editor \> "+self.originalFileName)
			self.winDone("new buffer added : "+self.originalFileName)
			return "break"
		else:
			self.winError("nothing to create new buffer")
			return 0
	
	def resetOriginal(self, event=None): # complete reset to first load
		if not self.originalImage and not self.tempImgSourceData:
			self.winError("nothing to reset")
			return 0	
			
		# reset all image params
		self.resetImageParams()
		self.resetImageParamsExtras()
		
		# new method from buffer, avoid hexadecimal change original
		bufferData = self.buffers[self.originalFileName]
		if bufferData[0]:
			self.originalImage = bufferData[1]
			self.tempImgSourceData.update({ 
				"original":		copy(bufferData[1]),
				"appleBinData":	copy(bufferData[2]),
				"pixel_map":	copy(bufferData[3]),
				"color_map":	copy(bufferData[4])
			})
			self.createNewAppleImageData()
			self.refreshAppleImage()
		else:
			self.tempImgSourceData = {}
			self.originalImage = bufferData[1]
			self.createNewAppleImage()
			self.refreshAppleImage()
			
		# clean title if params
		self.cleanTitleParams()
		# refresh
		self.createThumbnails()
		# show hide text
		self.imageMarkContent.set(self.appleImage.hideText)
		# get hex to field
		self.doHexadecimal()
		return 1
		
	def resetImageParams(self):
		# reset button, scale etc. but not pixel grid or rawhex
		self.ditherButton.select()
		self.geometry.set("GeometryOff")
		self.randNoiseButton.deselect()
		self.bitWise = "^0000000"
		self.bits = [0]*7
		self.bitWiseButton.configure(text=self.bitWise)
		self.warp.set(0)
		self.warpSoftButton.deselect()
		self.zigZagWarpButton.deselect()
		self.effect.set("X EffectOff")
		self.brightness.set(70)
		self.contrast.set(70)
		self.noise.set(0)
		self.shiftRight.set(0)
		self.shiftDown.set(0)
		self.randomMsbButton.deselect()
		self.linesBetween.set(0)
		self.linesByteVal.set(0)
		self.lineStep.set(40)
		self.appleScreenGreenButton.deselect()
		self.appleScreenAmberButton.deselect()
		self.bit_MSB_Button.deselect()
		self.mEdit.entryconfigure(4, label="Show Color Map")
		self.appleImage.showColorMap =  0
		# self.imageType.set("HGR") # not reset to keep always the same image mode  ?
		
	def resetImageParamsExtras(self):	
		# reset all last params
		self.selectColor = [0,0,0]
		self.pixelGridButton.deselect()
		self.pixelGridColSize.set(0)
		self.pixelGridRowSize.set(0)
		self.pixelGridColorButton.configure(background=self.defaultBgAlt)
		# self.rawHex.set(0) # seems more usefull to not reset hex mode
		self.rawHexFillColor.set("Black")
		
	def refreshAppleImageParams(self):
		if self.appleImage:
			# set vars of object
			self.appleImage.bit_MSB = 			self.bit_MSB.get()
			self.appleImage.random_MSB = 		self.random_MSB.get()
			self.appleImage.dither = 			self.dither.get()
			self.appleImage.brightness = 		self.brightness.get()
			self.appleImage.contrast = 			self.contrast.get()
			self.appleImage.effect = 			self.effect.get()
			self.appleImage.shiftRight = 		int(floor(self.shiftRight.get() * (40.0 / 100)))
			self.appleImage.shiftDown = 		int(floor(self.shiftDown.get() * (192.0 / 100)))
			self.appleImage.geometry = 			self.geometry.get()
			self.appleImage.warpSoft = 			self.warpSoft.get()
			self.appleImage.warp = 				int(floor(self.warp.get() * (200 / 100.0)))
			self.appleImage.zigZagWarp = 		self.zigZagWarp.get()
			self.appleImage.noise = 			self.noise.get()
			self.appleImage.randNoise = 		self.randNoise.get()
			self.appleImage.bitWise = 			self.bitWise
			self.appleImage.imageType = 		self.imageType.get()
			if self.linesBetween.get():
				self.appleImage.linesBetween = 	int(floor(192.0 / self.linesBetween.get()))
			else:
				self.appleImage.linesBetween = 0
			self.appleImage.linesByteVal = 		self.linesByteVal.get() # add lines byte val
			if self.lineStep.get() < 1: # avoid zero division and zero step
				lineStep = 						1 # default = 1
			else:
				lineStep = 						self.lineStep.get()
			lineStep = 							int(ceil(40.0 / lineStep)) # blocks frequency
			self.appleImage.lineStep = 			lineStep
			self.appleImage.mask = 				0
			self.appleImage.even = 				1
			hideText = self.imageMarkContent.get(); max = 512 # limit data to max
			if len(hideText) > max: hideText = hideText[0:max]
			self.imageMarkContent.set(hideText)
			self.appleImage.hideText = 			hideText
			return 1
		else:
			return 0
		
	def refreshAppleImage(self, event=None):
		if self.appleImage:
			self.refreshAppleImageParams()
	
			# --------------------------------------------- image from various sources
			try: # avoid tkinter error if clipboard used outside this application
				hexadecimalInClipboard = self.tkRoot.selection_get(selection = "CLIPBOARD")
			except:
				print "error: clipboard not available !"
				hexadecimalInClipboard = self.fieldCodeHexa.get(0.1, END)
				
			# when doHexadecimal() field content is copied to clipboard
			# if you edit the hexadecimal field the new image is translated from hexa code and a new data source image is created	
			if self.clipboardDataType == "monitor listing" and hexadecimalInClipboard != self.fieldCodeHexa.get(0.1, END):
				# print self.clipboardDataType,"hexadecimalInClipboard != self.fieldCodeHexa.get(0.1, END)"
				self.addUndoHexa(hexa=hexadecimalInClipboard) # save hexa code
				if self.getImageDataFromHexadecimal():# check if possible
					# update tempImgSourceData with hexa code
					binData, pixelMap = self.getImageDataFromHexadecimal()
					self.tempImgSourceData = {
						"original":		copy(self.originalImage),
						"appleBinData":	copy(binData),
						"pixel_map":	copy(pixelMap),
						"color_map":	copy(self.appleImage.appleBinColorMap),
					}
					self.setAppleImageSource() # remake image with data and new params
			elif self.isFromData(): # if the data is from other source than a pict
				# print "self.isFromData()"
				self.setAppleImageSource()
			elif self.appleImage.imSrc == None:
				# print "self.appleImage.imSrc == None"
				self.setAppleImageSource()
			else:
				# do image from image original
				pass
			# --------------------------------------------- 
			
			self.appleImage.makeAppleImage() # make image with the same imgToAppleHGR object
			# refresh thumbs
			self.createThumbnails()
			# show hide text
			self.imageMarkContent.set(self.appleImage.hideText)
			# get hex to field
			self.doHexadecimal()
			return 1
		else:
			return 0
			
	def isFromData(self):
		return self.buffers[self.originalFileName][0]
		
	def getEditorParams(self):
			# set vars of object
			params = 	[None]*20
			params[0] 	= self.bit_MSB.get()
			params[1] 	= self.random_MSB.get()
			params[2] 	= self.dither.get()
			params[3] 	= self.brightness.get()
			params[4] 	= self.contrast.get()
			params[5] 	= self.effect.get()
			params[6] 	= self.shiftRight.get()
			params[7] 	= self.shiftDown.get()
			params[8] 	= self.geometry.get()
			params[9] 	= self.warpSoft.get()
			params[10]	= self.warp.get()
			params[11] 	= self.zigZagWarp.get()
			params[12] 	= self.noise.get()
			params[13] 	= self.randNoise.get()
			params[14] 	= self.bitWise
			params[15] 	= self.imageType.get()
			params[16] 	= self.linesBetween.get()
			params[17] 	= self.linesByteVal.get()
			params[18] 	= self.lineStep.get()
			if len(self.imageMarkContent.get()) > 512: 
				self.imageMarkContent.set(self.imageMarkContent.get()[0:512])
			params[19] 			= self.imageMarkContent.get()
			return params
			
	def restoreEditorParams(self, params=None, hideText=1):
		if params:
			# set vars of object
			self.bit_MSB.set(				params[0])
			self.random_MSB.set(			params[1])
			self.dither.set(				params[2])
			self.brightness.set(			params[3])
			self.contrast.set(				params[4])
			self.effect.set(				params[5])
			self.shiftRight.set(			params[6])
			self.shiftDown.set(				params[7]) 
			self.geometry.set(				params[8])
			self.warpSoft.set(				params[9])
			self.warp.set(					params[10])
			self.zigZagWarp.set(			params[11])
			self.noise.set(					params[12])
			self.randNoise.set(				params[13])
			self.updateBitWise(bitWise=		params[14])
			self.imageType.set(				params[15])
			self.linesBetween.set(			params[16])
			self.linesByteVal.set(			params[17])
			self.lineStep.set(				params[18])
			if hideText:
				self.imageMarkContent.set(	params[19])
			return 1
		else:
			self.winError("no editor params to restore")
			return 0
			
	# button animation
	def resetOriginalFx(self, event=None): # with flash button
		self.resetOriginal()
		self.flashButton(w=self.resetButton)
		
	def refreshAppleImageFx(self, event=None): # with flash button
		self.refreshAppleImage()
		self.flashButton(w=self.refreshButton)
	
	# -------------------------------- hide text routines
	def extractHideTextFromFile(self, file=""):
		s = ""
		try:
			f = open(file,"rb")
			for i in xrange(0,8192,128):
				s += f.read(128)[120:128]
			f.close()
		except:
			self.winErrorFile("extract hide text, can't open or read "+file)
			return ""
			
		return "".join([x for x in s if x != "\x00"])
		
	def extractHideTextFromListing(self, bytes=bytes):
		hideTextBytes = []
		for i in xrange(0,8192,128):
			hideTextBytes.extend(bytes[i+120:i+128])
		
		hideText = map(chr, hideTextBytes)
		return "".join([x for x in hideText if x != "\x00"])
		
	def aIeHideTextInImage(self, appleBinImage=[]):
		# hide text in the 512 free bytes of the image
		hideText = [ord(c) for c in self.imageMarkContent.get()]
		k = 0 
		for i in xrange(0,8192,128):
			for j in xrange(0,8):
				if k < len(hideText):
					appleBinImage[i+120+j] = hideText[k]
					k += 1
				else:
					appleBinImage[i+120+j] = 0
					
		return appleBinImage
	
	# -------------------------------- result image manipulations
	def getAppleScreenImage(self): 
		chars = ""
		if self.appleScreenGreen.get(): # checkbutton green
			for e in self.appleImage.pixelMap:
				if e == 255:
					chars += self.green
				else:
					chars += self.black
			im = Image.frombytes("RGB", self.appleScr, chars)
		elif self.appleScreenAmber.get(): # checkbutton black/white on
			for e in self.appleImage.pixelMap:
				if e == 255:
					chars += self.orange
				else:
					chars += self.black
			im = Image.frombytes("RGB", self.appleScr, chars) # use RBG for color
		else:
			# get simulated Apple Screen Color (tv)
			im = self.getAppleScreenInColor(data=self.appleImage.appleBinData)
		
		return im
	
	def getAppleScreenInColor(self, data=[]):
		chars = ""
		pixels = []
		even = 2
		evenByte = 2
		for item in xrange(0,len(data)):
			sevenPixels = [] # reset each time
			byteStr = str(bin(data[item]))[2:].zfill(8)
			bit_MSB = byteStr[0] # Most significant bit
			byteStr = byteStr[1:] # pop Most significant bit
			byteStr = byteStr[::-1] # inverse bits order
			
			if item > 0 and item < len(data)-1:
				# prev byte
				byte = str(bin(data[item-1]))[2:].zfill(8)
				byte = byte[1:] # pop Most significant bit
				byte = byte[::-1] # inverse bits order
				prevBit = byte[6]
				
				# next byte
				byte = str(bin(data[item+1]))[2:].zfill(8)
				byte = byte[1:] # pop Most significant bit
				byte = byte[::-1] # inverse bits order
				nextBit = byte[0]
			else:
				prevBit = bit_MSB # or prevBit = "0"
				byte = str(bin(item+1))[2:].zfill(8)
				byte = byte[1:] # pop Most significant bit
				byte = byte[::-1] # inverse bits order
				nextBit = byte[0]
				
			byteStr = prevBit + byteStr + nextBit # byte with prev and next bit
			
			if evenByte % 2: # ----------------------------- even byte, because only 7 bit are in use the even/odd change every byte
				evenBit = 2
			else:
				evenBit = 1
				
			evenByte += 1
			
			# ---------------------------------------------- parse each bit for 1-8 including first bit of next byte
			for pos in xrange(1,9): # ---------------------- but not first (from previous byte), to compute bit from previous bits
				evenBit += 1
				bit = byteStr[pos] # ----------------------- select bit to test
				if bit_MSB == "0": # ----------------------- Most significant bit then even or odd colors
					# -------------------------------------- MSB 0 then even violet / odd green
					even_color = self.violet
					odd__color = self.green
				else:
					# -------------------------------------- MSB 1 then even blue / odd orange
					even_color = self.blue
					odd__color = self.orange
					
				if evenBit % 2 == 0: # --------------------- even bit then even_color
					color = even_color
				else:
					color = odd__color
					
				# ------------------------------------------ byteStr = "X"+"XXXXXXX"+"X" = prevBit + 7bits + nextBit	
				if bit == "1" and byteStr[pos-1] == "1": # - bits : "11"
					if sevenPixels: # ---------------------- check if previous pixel exist
						sevenPixels.pop() # ---------------- pop previous pixel
						sevenPixels.append(self.white) # --- replace previous pixel
					sevenPixels.append(self.white) # ------- add new pixel
				# ------------------------------------------ bits : "101"
				elif pos > 1 and bit == "1" and byteStr[pos-1] == "0" and byteStr[pos-2] == "1": 
					if sevenPixels:
						sevenPixels.pop()
						sevenPixels.append(color)
					sevenPixels.append(color)
				elif bit == "1" and byteStr[pos-1] == "0": # bits : "01"
					sevenPixels.append(color)
				elif bit == "0" and byteStr[pos-1] == "1": # bits : "10"
					sevenPixels.append(self.black)	
				elif bit == "0" and byteStr[pos-1] == "0": # bits : "00"
					sevenPixels.append(self.black)
				# ---------------------------------------------- end of checking bits
					
			sevenPixels.pop() # always pop last pixel because it's the next bit of the next byte
			pixels.extend(sevenPixels) # add seven pixels to array

		chars += "".join(pixels)
		return Image.frombytes("RGB", self.appleScr, chars)
		
	# -------------------------------- save images
	def saveFullData(self, event=None):
		fileName = tkDia.asksaveasfilename(
			parent=self.tkRoot, title="Save Full Working Data", initialdir=self.saveDiaRoot, 
			initialfile=self.getInitialFilename("png") , filetypes=[("Image png","*.png")]
		)
		if fileName and self.appleImage:
			self.saveDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			# save apple screen img as png
			im = self.appleScreenImage.resize(self.zoomScr) # png is 4 x bigger
			if self.pixelGrid.get():
				im = self.drawPixelGridImage(im=im)

			fileName = self.addExtension(fileName=fileName, ext="png")
			if not self.savePng(im=im, fileName=fileName):return self.winErrorFile("can't write "+fileName)
			
			# save bin	
			fileName = self.addExtension(fileName=fileName, ext="bin")
			binData = bytearray(self.appleImage.appleBinImage)
			if not self.saveBin(binData=binData, fileName=fileName):return self.winErrorFile("can't write "+fileName)
			
			# save data
			pixelMap = "["+",".join(map(str, self.appleImage.pixelMap))+"]"
			if self.appleImage.appleBinColorMap:
				colorMap = "["+"".join(",".join(map(str,self.appleImage.appleBinColorMap)))+"]"
			else:
				colorMap = "[]"
				
			data = pixelMap+","+colorMap+","+self.getEditorVarsText()
			fileName = self.addExtension(fileName=fileName, ext="dat")
			if not self.saveDat(data=data, fileName=fileName):return self.winErrorFile("can't write "+fileName)
			
			# save hexadecimal
			hexData = self.appleImage.getHexaCode()
			fileName = self.addExtension(fileName=fileName, ext="txt")
			if not self.saveHex(hexData=hexData, fileName=fileName):return self.winErrorFile("can't write "+fileName)
			
			# save pixel map as png black/white image
			im = self.pixelMapImage.resize(self.zoomScr) # png 4 x bigger
			pxMapfileName = self.addExtension(fileName=fileName, ext="bw.png")
			if not self.savePng(im=im, fileName=pxMapfileName):return self.winErrorFile("can't write "+pxMapfileName)
		else:
			self.winCancel("save full working data cancelled")
			return 0
		
		self.winDone(fileName[:-4]+".png (all), .bin, .dat and .txt (hexa) saved")
		
	def saveAll(self, event=None):
		fileName = tkDia.asksaveasfilename(
			parent=self.tkRoot, title="Save Picture Image PNG, Bin, Hexa, Dat", initialdir=self.saveDiaRoot, 
			initialfile=self.getInitialFilename("png") , filetypes=[("Image png","*.png")]
		)
		if fileName and self.appleImage:
			self.saveDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			# save apple screen img as png
			im = self.appleScreenImage.resize(self.zoomScr) # png is 4 x bigger
			if self.pixelGrid.get():
				im = self.drawPixelGridImage(im=im)
				
			fileName = self.addExtension(fileName=fileName, ext="png")
			if not self.savePng(im=im, fileName=fileName):return self.winErrorFile("can't write "+fileName)
			
			# save bin	
			fileName = self.addExtension(fileName=fileName, ext="bin")
			binData = bytearray(self.appleImage.appleBinImage)
			if not self.saveBin(binData=binData, fileName=fileName):return self.winErrorFile("can't write "+fileName)
			
			# save data
			pixelMap = "["+",".join(map(str, self.appleImage.pixelMap))+"]"
			if self.appleImage.appleBinColorMap:
				colorMap = "["+"".join(",".join(map(str,self.appleImage.appleBinColorMap)))+"]"
			else:
				colorMap = "[]"
				
			data = pixelMap+","+colorMap+","+self.getEditorVarsText()
			fileName = self.addExtension(fileName=fileName, ext="dat")
			if not self.saveDat(data=data, fileName=fileName):return self.winErrorFile("can't write "+fileName)
			
			# save hexadecimal
			hexData = self.appleImage.getHexaCode()
			fileName = self.addExtension(fileName=fileName, ext="txt")
			if not self.saveHex(hexData=hexData, fileName=fileName):return self.winErrorFile("can't write "+fileName)
		else:
			self.winCancel("save all cancelled")
			return 0
		
		self.winDone(fileName[:-4]+".png, .bin, .dat and .txt (hexa) saved")
		
	def exportTiff(self, event=None):
		fileName = tkDia.asksaveasfilename(
			parent=self.tkRoot, title="Export Picture to Tiff", initialdir=self.saveDiaRoot, 
			initialfile=self.getInitialFilename("tif") , filetypes=[("Image tiff","*.tif")]
		)
		if fileName and self.appleImage:
			self.saveDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			# save apple screen img as tif
			im = self.appleScreenImage
			fileName = self.addExtension(fileName=fileName, ext="tif")
			try:
				im.save(fileName)
				print fileName+" saved"
			except:
				return self.winErrorFile("can't write "+fileName)
		else:
			self.winCancel("export Tiff cancelled")
			return 0
		
		self.winDone(fileName[:-4]+".tif saved")
		
	def saveImageBin(self, event=None):
		fileName = tkDia.asksaveasfilename(
			parent=self.tkRoot, title="Save Image Binary File", initialdir=self.saveDiaRoot, 
			initialfile=self.getInitialFilename("bin"), filetypes=[("Image Apple Bin","*.bin")]
		)
		if fileName and self.appleImage:
			self.saveDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			# save bin
			binData = bytearray(self.appleImage.appleBinImage)
			fileName = self.addExtension(fileName=fileName, ext="bin", f=0)
			if not self.saveBin(binData=binData, fileName=fileName):return self.winErrorFile("can't write "+fileName)
		else:
			self.winCancel("save binary cancelled")
			return 0
			
		self.winDone("file "+fileName+" saved")
		
	def saveImageHex(self, event=None):
		fileName = tkDia.asksaveasfilename(
			parent=self.tkRoot, title="Save Hexadecimal Code File", initialdir=self.saveDiaRoot, 
			initialfile=self.getInitialFilename("txt"), filetypes=[("Hexadecimal text","*.txt")]
		)
		if fileName and self.appleImage:
			self.saveDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			hexData = self.appleImage.getHexaCode()
			fileName = self.addExtension(fileName=fileName, ext="txt", f=0)
			if not self.saveHex(hexData=hexData, fileName=fileName):return self.winErrorFile("can't write "+fileName)
		else:
			self.winCancel("save hexadecimal cancelled")
			return 0
			
		self.winDone("file "+fileName+" saved")
		
	def savePixelMapToPng(self, event=None):
		fileName = tkDia.asksaveasfilename(
			parent=self.tkRoot, title="Save Pixel Map", initialdir=self.saveDiaRoot, 
			initialfile=self.getInitialFilename("png") , filetypes=[("Image png","*.png")]
		)
		if fileName and self.appleImage:
			self.saveDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			im = self.pixelMapImage.resize(self.zoomScr) # png is 4 x bigger
			fileName = self.addExtension(fileName=fileName, ext="png")
			if not self.savePng(im=im, fileName=fileName):return self.winErrorFile("can't write "+fileName)
		else:
			self.winCancel("save PixelMap to Png cancelled")
			return 0

		self.winDone(fileName[:-4]+".png saved")
		
	def saveParamsToFile(self, event=None):
		fileName = tkDia.asksaveasfilename(
			parent=self.tkRoot, title="Save Params Dat File", initialdir=self.saveDiaParamsRoot, 
			initialfile=self.getInitialFilename("dat") , filetypes=[("Dat File","*.dat")]
		)
		if fileName and self.appleImage:
			self.saveDiaParamsRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			data = "[],[],"+self.getEditorVarsText()
			fileName = self.addExtension(fileName=fileName, ext="dat")
			if not self.saveDat(fileName=fileName, data=data):return self.winErrorFile("can't write "+fileName)
		else:
			self.winCancel("save Params Dat File cancelled")
			return 0

		self.winDone(fileName[:-4]+".dat saved")
		
	def savePng(self, im=None, fileName=None):
		try:
			im.save(fileName, "PNG")
			print fileName+" saved"
			return 1
		except:
			return 0
			
	def savePdf(self, im=None, fileName=None):
		try:
			im.save(fileName, "PDF", resolution=100.0)
			print fileName+" saved"
			return 1
		except:
			return 0
		
	def saveBin(self, binData=None, fileName=None):
		try:
			f = open(fileName,"wb")
			f.write(binData)
			f.close()
			print fileName+" saved"
			return 1
		except:
			return 0
		
	def saveHex(self, hexData=None, fileName=None):
		try:
			f = open(fileName,"w")
			f.write(hexData)
			f.close()
			print fileName+" saved"
			return 1
		except:
			return 0
		
	def saveDat(self, data=None, fileName=None):
		try:
			f = open(fileName,"w")
			f.write(data)
			f.close()
			print fileName+" saved"
			return 1
		except:
			return 0
		
	def getInitialFilename(self, ext=None):
		if not self.originalFileName:return ""
		fileName = self.originalFileName
		if fileName[-4] == ".": # already have extension, change it
			fileName = fileName[0:-4]
			
		initialFileName = path.basename(fileName)+"-"+strftime("%Y-%m-%d-%H-%M-%S", localtime())	
		initialFileName = self.addExtension(fileName=initialFileName, ext=ext)
		return initialFileName
		
	def addExtension(self, fileName=None, ext=None, f=1):# don't work with long extensions ex jpeg
		if not fileName or not ext: return 0
		if f == 1: # force adding extension default
			if fileName[-4] == ".": # already have extension, change it
				fileName = fileName[0:-3]+ext
			else:
				fileName = fileName+"."+ext
		else:
			if fileName[-4] == ".": # "." already have extension, don't change user choice
				pass
			else:
				fileName = fileName+"."+ext
				
		return fileName
		
	# -------------------------------- png decoration apple screen routines
	def drawPixelGridImage(self, im=None):
		draw = ImageDraw.Draw(im)
		color = []
		for c in self.selectColor:
			if c < 245: 
				color.append(c+10) # create a second darker color
			else:
				color.append(c)
				
		color = tuple(color)
		selectColor = tuple(self.selectColor)
		
		if self.pixelGridRowSize.get():
			height = self.pixelGridRowSize.get()+1
			for x in xrange(0, im.size[0], height*2):
				x1 = x; y1 = 0
				x2 = x; y2 = im.size[1]
				for i in xrange(1,height):
					draw.line((x1,y1,x2,y2), fill=color)
					draw.line((x1+1,y1,x2+1,y2), fill=selectColor)
					x1 += 1; x2 += 1;
				
		if self.pixelGridColSize.get():
			width = self.pixelGridColSize.get()+1
			for y in xrange(0, im.size[1], width*2):
				x1 = 0; 			y1 = y
				x2 = im.size[0]; 	y2 = y
				for i in xrange(1,width):
					draw.line((x1,y1,x2,y2), fill=color)
					draw.line((x1,y1+1,x2,y2+1), fill=selectColor)
					y1 += 1; y2 += 1
			
		return im
		
	# -------------------------------- colors
	def	setPixelGridColor(self):
		color = tkColorChooser.askcolor()
		if color[0]:
			self.selectColor = color[0]
			self.pixelGridColorButton.configure(background=color[1])
			return 1
		else:
			self.selectColor = [0,0,0]
			self.pixelGridColorButton.configure(background=self.defaultBgColor)
			return 0
			
	def chooseColor(self):
		color = tkColorChooser.askcolor()
		if color[0]:
			self.selectColor = color[0]
			return 1
		else:
			return 0
			
	def createAppleColors(self):# Apple HGR colors in chars
		self.green = "".join(map(chr, self.A_Colors["green"]))
		self.violet = "".join(map(chr, self.A_Colors["violet"]))
		self.blue = "".join(map(chr, self.A_Colors["blue"]))
		self.orange = "".join(map(chr, self.A_Colors["orange"]))
		self.black = "".join(map(chr, self.A_Colors["black"]))
		self.white = "".join(map(chr, self.A_Colors["white"]))
	
	# -------------------------------- Apple Image Editor apple bin routines
	def aIeCreateAppleBin(self, bytes=bytes):
		appleBinImage = [0]*9162
		x = y = s_1 = s_2 = s_3 = 0
		for i in xrange(0,192): # 192 lines
			if i != 0: # avoid first 0
				if i % 8  == 0: s_1 = 0
				if i % 8  == 0: s_2 += 128
				if i % 64 == 0: s_2 = 0
				if i % 64 == 0: s_3 += 40
				
			x = s_1 + s_2 + s_3
			y = x + 40
			
			a = i*40; b = a+40; appleBinImage[x:y] = bytes[a:b]
			
			s_1 += 1024
		
		return appleBinImage
	
	# -------------------------------- others
	def addUndoHexa(self, hexa=""):
		if len(self.undoHexa) > 1:
			if self.undoHexa[1] != hexa:
				self.undoHexa.pop(0)
				self.undoHexa.append(hexa)
		else:
			self.undoHexa.append(hexa)
		
	def undoBack(self, event=None):
		if len(self.undoHexa) > 1:
			self.fieldCodeHexa.delete(0.1, END)
			self.fieldCodeHexa.insert(INSERT, self.undoHexa[-1])
			return self.refreshAppleImage()
		elif self.undoHexa:
			self.fieldCodeHexa.delete(0.1, END)
			self.fieldCodeHexa.insert(INSERT, self.undoHexa[0])
			return self.refreshAppleImage()
		
	def winError(self, m=""):
		print m
		tkMsg.showerror("Error", m)
		
	def winErrorFile(self, m=""):
		print m
		tkMsg.showerror("Error File", m)
		
	def winCancel(self, m=""):
		print m
		tkMsg.showinfo("Cancel", m)
		
	def winDone(self, m=""):
		print m
		tkMsg.showinfo("Done", m)
		
	def upperCase(self, w=None, r=0):
		if r == 0:w.set(w.get().upper())
		
	def showColorMap(self, event=None):
		if not self.appleImage:return 0
		if self.appleImage.showColorMap == 0:
			self.appleImage.showColorMap =  1
			self.mEdit.entryconfigure(4, label="Hide Color Map")
		else:
			self.mEdit.entryconfigure(4, label="Show Color Map")
			self.appleImage.showColorMap =  0
		
		# reset appleImage
		self.appleImage.makeAppleImage()
		self.refreshAppleImage()
		
	def sizOk(self, arr, siz):
		if len(arr) != siz:
			self.winError("array length error expect "+str(siz)+" get "+str(len(arr)))
			return 0
		else:
			return 1
			
	def invBool(self, v=False):
		if v:
			return False
		else:
			return True
			
	def arrayPick(self,arr=[],s=10):
		result = []
		for i in xrange(0,len(arr),s):
			result.append(arr[i])
			
		return result
			
	def flashButton(self, w=None):
		# function after seems not working well under linux ubuntu
		#w.config(bg="green1")
		#self.tkRoot.after(100, lambda: w.config(bg="green3"))
		#w.config(relief=SUNKEN)
		#self.tkRoot.after(100, lambda: w.config(relief=RAISED))
		w.flash()
		
	def widgetDeselect(self, w=None):
		w.deselect()
		
	def zoomTextIn(self, event=None):
		self.monoFontSize += 1
		self.customMonoFont.configure(size=self.monoFontSize)
		return "break" # prevent for bind->handler in Text class
	
	def zoomTextOut(self, event=None):
		self.monoFontSize -= 1
		self.customMonoFont.configure(size=self.monoFontSize)
		return "break" # prevent for bind->handler in Text class
		
	def selectFieldContent(self, event=None):
		self.fieldCodeHexa.tag_add(SEL, "1.0", END)
		self.fieldCodeHexa.focus_set()
		return "break"
		
	def selectImageMarkContent(self, event=None):
		self.imageMark.select_range("0", END)
		self.imageMark.focus_set()
		return "break"
		
	def doCompleteListing(self, event=None):
		if self.appleImage:
			hexaCode = "HGR\nVTAB(22)\nCALL -151\n"+self.appleImage.getHexaCode()+"\n3D0G\nPOKE -16302,0\n"
			self.copyHexaToClipboard(data=hexaCode, dataType="complete basic monitor listing")
			self.winDone("complete listing copied to clipboard")
			return 1
		else:
			self.winError("no apple image")
			return 0
		
	def saveImg(self, im=None, f="test.png", r=(0,0)):
		if r != (0,0):
			im = im.resize(r)
		try:
			im.save(f,"PNG")
			return 1
		except:
			return 0
			
	def openImageGimp(self, event=None):
		# open image
		fileName = tkDia.askopenfilename(parent=self.tkRoot, title="Open Image File", initialdir=self.openDiaRoot, filetypes=[("Image",("*.png","*.jpg","*.jpeg","*.gif")), ("all files", ".*")])
		if fileName:
			self.openDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			try:
				# call([gimp, fileName])
				Popen([gimp, fileName])
			except:
				self.winErrorFile("can't open "+fileName)
				return 0
		else:
			self.winCancel("open image with gimp cancelled")
			return 0
		return 1
		
	def openAppleWin(self, event=None):
		# open image
		try:
			Popen([appleWin])
		except:
			self.winErrorFile("can't open "+fileName)
			return 0
	# -------------------------------- zoom images
	def zoomImageResult(self, event=None):
		if not self.appleScreenImage: return 0
		if self.pixelGrid.get():
			factor = 1
			w, h = self.zoomScr
			im = self.appleScreenImage.resize((w, h))
			im = self.drawPixelGridImage(im=im) # add pixel grid
		else:
			factor = 4
			im = self.appleScreenImage
		
		self.zoomImage(im=im, factor=factor)
		
	def zoomImageOriginal(self, event=None):
		if not self.originalImage: return 0
		self.zoomImage(im=self.originalImage)
		
	def zoomPixelMap(self, event=None):
		if not self.appleImage: return 0
		self.zoomImage(im=self.pixelMapImage)
		
	def zoomImage(self, im=None, factor=4):
		try:
			self.winZoom.destroy()
		except:
			pass
			
		self.winZoom = Toplevel(self.tkRoot)
		self.winZoom.withdraw() # hide win
		self.winZoom.protocol("WM_DELETE_WINDOW", self.closeZoom)	
		self.winZoom.title("Zoom Image "+self.originalFileName)
		self.winZoom.configure(background="black")
		self.imageZ = self.zoomResize(im=im, factor=factor)
		self.imageZoom = ImageTk.PhotoImage(self.imageZ)
		w, h = self.imageZ.size
		zoomCanvas = Canvas(self.winZoom, width=w, height=h, cursor="hand1")
		zoomCanvas.create_image(1,1, anchor=NW, image=self.imageZoom)
		zoomCanvas.grid(row=0, column=0, padx=5, pady=5)
		zoomCanvas.bind("<ButtonRelease-1>", self.closeZoom)
		self.winZoom.grid_columnconfigure(0, weight=1)
		self.winZoom.grid_rowconfigure(0, weight=1)
		if self.winZoomPos: self.winZoom.geometry(self.winZoomPos)
		self.winZoom.deiconify() # show win
		self.winZoom.mainloop()
		
	def zoomResize(self, im=None, factor=1):
		width, height = im.size
		return im.resize((width*factor, height*factor))
		
	def zoomfitScreen(self, im=None, width=0, height=0): # if device screen less than image size
		b = 70; w_f = 1; h_f = 1
		if width > self.tkRoot.winfo_screenwidth():
			w_f = (self.tkRoot.winfo_screenwidth() - b) / float(width) 
			
		if height > self.tkRoot.winfo_screenheight():
			h_f = (self.tkRoot.winfo_screenheight() - b) / float(height)
		
		if w_f < h_f:
			width, height = width*w_f, height*w_f
		else:
			width, height = width*h_f, height*h_f	
		
		return im.resize((int(width), int(height)))
		
	def closeZoom(self, event=None):
		self.winZoomPos = self.winZoom.geometry()
		self.winZoom.destroy()
	
	# -------------------------------- animation
	def openAnimation(self, event=None, save=True):
		try:
			self.winAnimation.destroy()
		except:
			pass
				
		self.winAnimation = Toplevel(self.tkRoot)
		self.winAnimation.withdraw() # hide win	
		self.winAnimation.protocol("WM_DELETE_WINDOW", self.closeWinAnimation)
		self.winAnimation.title("Animation")
		
		# set menu
		menubar = Menu(self.winAnimation)
		# file
		mAnim = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="File", menu=mAnim)
		mAnim.add_command(label="Play Animation", command=self.playAnimation)
		mAnim.add_command(label="Play Animation and save to files png", command=self.saveAnimationFiles)
		mAnim.add_command(label="Play Animation and save to files bin", command=self.saveAnimationFilesBin)
		if path.isfile(ffmpeg): 
			mAnim.add_command(label="Play Animation and save to files png and video", command=self.saveAnimationFilesVideo)
			mAnim.add_command(label="Play Animation and save to files png and gif", command=self.saveAnimationFilesGif)
			mAnim.add_command(label="Add sound to Animation", command=self.animationAddSound)
		mAnim.add_separator()
		mAnim.add_command(label="Quit", command=self.closeWinAnimation)
		
		# set menu
		self.winAnimation.configure(menu=menubar)

		self.animLabel = Label(self.winAnimation, font=self.customFontBold, text="Animation Tool                                                                                               :")
		self.animInfos = Label(self.winAnimation, font=self.customFont,     text="Infos")
		self.animTimesLabel = Label(self.winAnimation, font=self.customFontBold, text="Images")
		self.animTimesVal = StringVar()
		self.animTimesVal.set(32)
		self.animTimes = Entry(self.winAnimation, textvariable=self.animTimesVal, width=5, font=self.customFont)
		self.animCounterLabel = Label(self.winAnimation, font=self.customFontBold, text="Counter")
		self.animTimesCounter = StringVar()
		self.animTimesCounter.set(0)
		self.animCounter = Label(self.winAnimation, textvariable=self.animTimesCounter, width=5, font=self.customFont, anchor=W)
		self.animLabelDescription = Label(self.winAnimation, text="0% = sometimes-often-always = 100%", width=5, font=self.customFontBold, anchor=W)
		self.animAleaByteVal = Scale(self.winAnimation, from_=0, to=255, showvalue=0, label="Byte", font=self.customFont, orient=HORIZONTAL)
		self.animAleaBetweenLinesVal = Scale(self.winAnimation, from_=0, to=100, showvalue=0, label="Lines", font=self.customFont, orient=HORIZONTAL)
		self.animAleaByteLinesVal = Scale(self.winAnimation, from_=0, to=255, showvalue=0, label="Byte Lines", font=self.customFont, orient=HORIZONTAL)
		self.animAleaBrightnessVal = Scale(self.winAnimation, from_=0, to=100, showvalue=0, label="Brightness", font=self.customFont, orient=HORIZONTAL)
		self.animAleaContrastVal = Scale(self.winAnimation, from_=0, to=100, showvalue=0, label="Contrast", font=self.customFont, orient=HORIZONTAL)
		self.animAleaBitWise = Scale(self.winAnimation, from_=0, to=127, showvalue=0, label="Bitwise", font=self.customFont, orient=HORIZONTAL)
		self.animRight = Scale(self.winAnimation, from_=0, to=100, showvalue=0, label="Decal Right", font=self.customFont, orient=HORIZONTAL)
		self.animLeft = Scale(self.winAnimation, from_=0, to=100, showvalue=0, label="Decal Left", font=self.customFont, orient=HORIZONTAL)
		self.animDown = Scale(self.winAnimation, from_=0, to=100, showvalue=0, label="Decal Down", font=self.customFont, orient=HORIZONTAL)
		self.animUp = Scale(self.winAnimation, from_=0, to=100, showvalue=0, label="Decal Up", font=self.customFont, orient=HORIZONTAL)
		self.animEffect =  Scale(self.winAnimation, from_=0, to=100, showvalue=0, label="Effect", font=self.customFont, orient=HORIZONTAL)
		self.animGeometry =  Scale(self.winAnimation, from_=0, to=100, showvalue=0, label="Geometry", font=self.customFont, orient=HORIZONTAL)
		self.animWarp =  Scale(self.winAnimation, from_=0, to=100, showvalue=0, label="Warp", font=self.customFont, orient=HORIZONTAL)
		
		animGrid = [
			# widget								r  c  rs cs sticky
			[self.animLabel, 						0, 0, 1, 2, N+W],
			[self.animTimesLabel, 					1, 0, 1, 1, N+E],
			[self.animTimes, 						1, 1, 1, 1, N+W],
			[self.animCounterLabel, 				2, 0, 1, 1, N+E],
			[self.animCounter, 						2, 1, 1, 1, N+W],
			[self.animLabelDescription, 			3, 0, 1, 2, N+W+E],
			
			[self.animAleaByteVal, 					4, 0, 1, 1, N+W+E],
			[self.animAleaBitWise, 					4, 1, 1, 1, N+W+E],
			
			[self.animAleaBetweenLinesVal, 			5, 0, 1, 1, N+W+E],
			[self.animAleaByteLinesVal, 			5, 1, 1, 1, N+W+E],
			
			
			[self.animAleaBrightnessVal, 			6, 0, 1, 1, N+W+E],
			[self.animAleaContrastVal, 				6, 1, 1, 1, N+W+E],
			
			[self.animLeft, 						7, 0, 1, 1, N+W+E],
			[self.animRight, 						7, 1, 1, 1, N+W+E],
			
			[self.animDown, 						8, 0, 1, 1, N+W+E],
			[self.animUp, 							8, 1, 1, 1, N+W+E],
			
			[self.animEffect, 						9, 0, 1, 1, N+W+E],
			[self.animGeometry, 					9, 1, 1, 1, N+W+E],
			[self.animWarp, 						10, 0, 1, 1, N+W+E],
			[self.animInfos, 						11, 0, 1, 2, N+W]
		]
		for w in animGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
		
		if self.winAnimationPos:self.winAnimation.geometry(self.winAnimationPos)
		self.winAnimation.deiconify() # show win
		self.winAnimation.mainloop()
		
		
	def closeWinAnimation(self, event=None):
		self.winAnimationPos = self.winAnimation.geometry()
		self.winAnimation.destroy()
		
	def saveAnimationFilesGif(self, event=None):
		dirName = tkDia.askdirectory(initialdir=self.tkRoot,title="select directory")
		if not dirName: return self.winCancel("cancel choose directory")
		new = "/v-"+str(randint(100000,999999))+".gif"	
		self.animInfos.configure(text="Save files to gif,"+self.remaining())
		dirName = self.playAnimation(save=True, directory=dirName)
		call([ffmpeg, '-f', 'image2', '-i', dirName+'/img-%5d.png', dirName+new])
			
		return self.winDone("animation saved in "+dirName)
		
	def saveAnimationFilesVideo(self, event=None):
		dirName = tkDia.askdirectory(initialdir=self.tkRoot,title="select directory")
		if not dirName: return self.winCancel("cancel choose directory")
		new = "/v-"+str(randint(100000,999999))+".avi"
		self.animInfos.configure(text="Save files to video,"+self.remaining())
		dirName = self.playAnimation(save=True, size=self.zoomScr, directory=dirName)
		call([ffmpeg, '-f', 'image2', '-i', dirName+'/img-%5d.png', dirName+new])
			
		return self.winDone("animation saved in "+dirName)
		
	def animationAddSound(self, event=None):
		new = "/"+str(randint(100000,999999))+".avi"
		self.animInfos.configure(text="Add sound to video")
		videoFileName = tkDia.askopenfilename(parent=self.tkRoot, title="Get Video File", initialdir=self.openDiaRoot, filetypes=[("Sound",("*.avi")), ("all files", ".*")])
		if not videoFileName:
			return self.winCancel("cancel add audio")
		else:
			self.openDiaRoot = path.dirname(videoFileName)
			self.storeVars()
			
		audioFileName = tkDia.askopenfilename(parent=self.tkRoot, title="Get Audio File", initialdir=self.openDiaRoot, filetypes=[("Sound",("*.mp3")), ("all files", ".*")])
		if audioFileName:
			call([ffmpeg, '-i', videoFileName, '-i', audioFileName, '-map', '0', '-map', '1', '-codec', 'copy', '-shortest', self.openDiaRoot+new])
		else:
			return self.winCancel("cancel add audio")
		
		return self.winDone("sound "+audioFileName+" added to video")
	
	def saveAnimationFiles(self, event=None):
		dirName = tkDia.askdirectory(initialdir=self.tkRoot,title="select directory")
		if not dirName: return self.winCancel("cancel choose directory")
		self.animInfos.configure(text="Save files"+self.remaining())
		self.playAnimation(save=True, directory=dirName)
		return self.winDone("animation saved in "+dirName)
		
	def saveAnimationFilesBin(self, event=None):
		dirName = tkDia.askdirectory(initialdir=self.tkRoot,title="select directory")
		if not dirName: return self.winCancel("cancel choose directory")
		self.animInfos.configure(text="Save files"+self.remaining())
		self.playAnimation(save=True, directory=dirName, ext=".bin")
		return self.winDone("animation saved in "+dirName)
		
	def playAnimation(self, event=None, save=False, size=(), directory=None, ext=".png"):
		byte = int(self.noise.get())
		linesByte = int(self.linesByteVal.get())
		linesBetween = int(self.linesBetween.get())
		brightness = int(self.brightness.get())
		contrast = int(self.contrast.get())
		right = int(self.shiftRight.get())
		left = int(self.shiftRight.get())
		down = int(self.shiftDown.get())
		up = int(self.shiftDown.get())
		warp = int(self.warp.get())
		bitWise = int(self.bitWise[1:], 2)
		dir = None
		
		if save == True:
			fileName = path.basename(self.originalFileName)
			if directory:
				dir = directory+"/"+fileName[:-4]+"-"+str(int(time()))+"/"
			elif path.dirname(self.originalFileName):
				dir = path.dirname(self.originalFileName)+"/"+fileName[:-4]+"-"+str(int(time()))+"/"
			else:
				dir = getcwd()+"/"+fileName[:-4]+"-"+str(int(time()))+"/"
				
			if not path.isdir(dir):
					mkdir(dir)
		
		for t in xrange(0, int(self.animTimes.get())):
			if self.animWarp.get() == 100:
				self.warp.set(warp)
				warp += 1
				if warp == 100:
					warp = 0
			elif self.animWarp.get() > 0:
				if randint(0,100) > (100 - int(self.animWarp.get())):
					self.warp.set(randint(0,100))
					
			if self.animRight.get() > 1 and self.animRight.get() < 100:
				if right >= 100:
					right = 0
				self.shiftRight.set(right)
				right += self.animRight.get()
			elif self.animRight.get() == 100:
				self.shiftRight.set(randint(0,100))
				
			if self.animLeft.get() > 1 and self.animLeft.get() < 100:
				if left <= 0:
					left = 100
				self.shiftRight.set(left)
				left -= self.animLeft.get()
			elif self.animLeft.get() == 100:
				self.shiftRight.set(randint(0,100))
				
			if self.animDown.get() > 1 and self.animDown.get() < 100:
				if down >= 100:
					down = 0
				self.shiftDown.set(down)
				down += self.animDown.get()
			elif self.animDown.get() == 100:
				self.shiftDown.set(randint(0,100))
				
			if self.animUp.get() > 1 and self.animUp.get() < 100:
				if up <= 0:
					up = 100
				self.shiftDown.set(up)
				up -= self.animUp.get()
			elif self.animUp.get() == 100:
				self.shiftDown.set(randint(0,100))
			
			if self.animAleaBitWise.get() == 127:
				bitWiseVal = self.bitWiseOperator+ "%07d" % int(bin(bitWise)[2:])
				self.updateBitWise(bitWise=bitWiseVal)
				bitWise += 1
				if bitWise == 127:
					bitWise = 0
			elif self.animAleaBitWise.get() > 0:
				if randint(0,127) > (127 - int(self.animAleaBitWise.get())):
					bitWiseVal = self.bitWiseOperator+ "%07d" % int(bin(randint(0,127))[2:])
					self.updateBitWise(bitWise=bitWiseVal)
			
			if self.animAleaByteVal.get() == 255:
				self.noise.set(byte)
				byte += 1
				if byte == 255:
					byte = 0
			elif self.animAleaByteVal.get() > 0:
				if randint(0,255) > (255 - int(self.animAleaByteVal.get())):
					self.noise.set(randint(0,256))
					
			if self.animAleaBetweenLinesVal.get() == 100:
				self.linesBetween.set(linesBetween)
				linesBetween += 1
				if linesBetween == 100:
					linesBetween = 0
			elif self.animAleaBetweenLinesVal.get() > 0:
				if randint(0,100) > (100 - int(self.animAleaBetweenLinesVal.get())):
					self.linesBetween.set(randint(0,100))				
					
			if self.animAleaByteLinesVal.get() == 255:
				self.linesByteVal.set(linesByte)
				linesByte += 1
				if linesByte == 255:
					linesByte = 0
			elif self.animAleaByteLinesVal.get() > 0:
				if randint(0,255) > (255 - int(self.animAleaByteLinesVal.get())):
					self.linesByteVal.set(randint(0,256))				
					
			if self.animAleaBrightnessVal.get() == 100:
				self.brightness.set(brightness)
				brightness += 1
				if brightness == 100:
					brightness = 0
			elif self.animAleaBrightnessVal.get() > 0:
				if randint(0,100) > (100 - int(self.animAleaBrightnessVal.get())):
					self.brightness.set(randint(0,100))
				
			if self.animAleaContrastVal.get() == 100:
				self.contrast.set(contrast)
				contrast += 1
				if contrast == 100:
					contrast = 0
			elif self.animAleaContrastVal.get() > 0:
				if randint(0,100) > (100 - int(self.animAleaContrastVal.get())):
					self.contrast.set(randint(0,100))
					
			if self.animEffect.get() > 1:
				if randint(0,100) > (100 - int(self.animEffect.get())):
					self.effect.set(choice([	"X EffectOff", "green", "orange", "violet", "blue", "white", "random", "even reverse",
												"inverse", "rnd noise", "abs(~byte)", "Mess image", "Zig Zag", "Zig Zag x2", "Zig Zag x3",
												"Zig Zag x4", "Zig Zag x5", "Zig Zag x6", "alt stripes",
												"v-lines inv", "v-lines rbow", "v-lines db inv", "rnd average", "dark v-lines", "free filter"]))
											
			if self.animGeometry.get() > 1:
				if randint(0,100) > (100 - int(self.animGeometry.get())):
					self.geometry.set(choice([	"GeometryOff", "UpsideDown", "Flip", "Down+Flip", "Mirror H", 
												"Mirror H inv", "Mirror V", "Mirror V inv", "Mirror H+V"]))
			
			self.animTimesCounter.set((t+1))			
			self.refreshAppleImage()
			# update windows
			self.tkRoot.update_idletasks()
			self.winAnimation.update()
			
			if save == True:
				num = "%05d" % (t+1)
				file = "img-"+num+ext
				dest = dir+file
					
				if size:
					im = self.appleScreenImage.resize(size)
				else:
					im = self.appleScreenImage
				
				if ext == ".png":
					# im.save(dest, "PNG")
					self.savePng(im,dest)
				if ext == ".bin":
					binData = bytearray(self.appleImage.appleBinImage)
					self.saveBin(binData=binData, fileName=dest)
				
				print t,dest+" saved"
				sleep(1)
				
		return dir
		
	def remaining(self):
		if int(self.animTimes.get()) > 60:
			return " time "+str(int(self.animTimes.get()) / 60)+" min"
		else:
			return " time "+self.animTimes.get()+" sec"
			
	# -------------------------------- GR tool	
	def openGR_Editor(self, event=None):
		try:
			self.winGR.destroy()
		except:
			pass
				
		self.winGR = Toplevel(self.tkRoot)	
		self.winGR.withdraw() # hide win	
		self.winGR.protocol("WM_DELETE_WINDOW", self.closeWinGR)
		self.winGR.title("Apple Low Res Image Editor")
		
		# set menu
		menubar = Menu(self.winGR)
		# file
		mFile = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="File", menu=mFile)
		mFile.add_command(label="Open Source Image File", command=self.openGR_Image)
		mFile.add_command(label="Use Apple Image Editor Source Image", command=self.useHGR_ImageSource)
		mFile.add_command(label="Save AppleSoft Listing", command=self.saveGR_Basic)
		mFile.add_command(label="Copy Basic to Clipboard", command=self.GR_basicCopyClipboard)
		mFile.add_separator()
		mFile.add_command(label="Save image as png", command=self.saveGR_image)
		mFile.add_separator()
		mFile.add_command(label="Quit", command=self.closeWinGR)
		
		# set menu
		self.winGR.configure(menu=menubar)
		
		self.GR_imageType = StringVar(); self.GR_imageType.set("Color")
		GR_imageTypeOptionMenu = OptionMenu(self.winGR, self.GR_imageType, "Color", "Black/White", command=self.refreshGR_Image)
		GR_imageTypeOptionMenu.configure(font=self.customFont)
		
		self.GR_brightness = Scale(self.winGR, from_=0, to=100, showvalue=0, label="Brightness", font=self.customFont, orient=HORIZONTAL)
		self.GR_brightness.set(70)

		self.GR_contrast = Scale(self.winGR, from_=0, to=100, showvalue=0, label="Contrast", font=self.customFont, orient=HORIZONTAL)
		self.GR_contrast.set(70)
		
		self.fieldGR = Text(self.winGR, font=self.customMonoFont, height=20, relief=SUNKEN, wrap=WORD, undo=True)
		self.fieldGR.bind("<Control-a>",self.selectGR_FieldContent)
		
		scrollbarGR = Scrollbar(self.winGR)
		scrollbarGR.configure(command=self.fieldGR.yview)
		self.fieldGR.config(yscrollcommand=scrollbarGR.set)
		
		self.GR_labelOriginal = Label(self.winGR, font=self.customFontBold, text="Original Image")
		self.canGR_Original = Canvas(self.winGR, width=self.appleScr[0], height=self.appleScr[1], borderwidth=1, background="white")
		
		self.GR_labelFinal = Label(self.winGR, font=self.customFontBold, text="Low Res Image")
		self.canGR_Final = Canvas(self.winGR, width=self.appleScr[0], height=self.appleScr[1]-32, cursor="hand1", borderwidth=1, background="white")
		self.canGR_Final.bind("<Button-1>",self.zoomGR_Final)
		
		GR_Grid = [
			# widget							r  c  rs cs sticky
			[GR_imageTypeOptionMenu, 			0, 0, 1, 1, N+W],
			[self.GR_brightness, 				1, 0, 1, 1, N+W+E],
			[self.GR_contrast, 					1, 1, 1, 1, N+W+E],
			[self.GR_labelOriginal, 			2, 0, 1, 1, N+W],
			[self.GR_labelFinal, 				2, 1, 1, 1, N+W],
			[self.canGR_Original, 				3, 0, 1, 1, N+W],
			[self.canGR_Final, 					3, 1, 1, 1, N+W],
			[self.fieldGR, 						4, 0, 1, 2, N+W+E],
			[scrollbarGR, 						4, 3, 1, 1, N+S]
		]
		for w in GR_Grid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
			
		for widget in [self.GR_brightness, self.GR_contrast]:
			widget.bind("<ButtonRelease-1>", self.refreshGR_Image) # on release mouse refresh image
			
		self.winGR.grid_columnconfigure(1, weight=1)
			
		if self.originalImage and not self.imageGR:
			self.originalGR_Image = copy(self.originalImage)
			self.imageGR = ImgToAppleGR()
			
		self.refreshGR_Image()
			
		if self.winGR_Pos:self.winGR.geometry(self.winGR_Pos)
		self.winGR.deiconify() # show win
		self.winGR.mainloop()
		
	def selectGR_FieldContent(self, event=None):
		self.fieldGR.tag_add(SEL, "1.0", END)
		self.fieldGR.focus_set()
		return "break"
		
	def zoomGR_Final(self, event=None):
		if self.GR_FinalThumbnailX2 == 1:
			self.GR_FinalThumbnailX2 = 0
			self.canGR_Final.configure(width=self.appleScr[0], height=self.appleScr[1]-32)
		else:
			self.GR_FinalThumbnailX2 = 1
			self.canGR_Final.configure(width=self.appleScr[0]*2, height=(self.appleScr[1]-32)*2)
			
		self.refreshGR_Image()
		
	def useHGR_ImageSource(self, event=None):
		if self.originalImage:
			self.originalGR_Image = copy(self.originalImage)
			self.imageGR = ImgToAppleGR()
			self.refreshGR_Image()
			return 1
		else:
			self.winError("no image source")
			return 0
	
	def closeWinGR(self, event=None):
		self.winGR_Pos = self.winGR.geometry()
		self.winGR.destroy()
		if self.appleImage:
			self.copyHexaToClipboard()
		
	def refreshGR_Image(self, event=None):
		if self.imageGR:
			# original image -> self.originalImage
			self.canGR_OriginalImage = ImageTk.PhotoImage(self.originalGR_Image)
			# always need an image object ex: self.xxx in self.canvasXxx, why ?
			self.canGR_Original.create_image(3,3, anchor= NW, image=self.canGR_OriginalImage)
			
			# pixel final image -> from imgToAppleGR final
			self.imageGR.imSrc = self.originalGR_Image
			self.imageGR.imageType = self.GR_imageType.get()
			self.imageGR.brightness = self.GR_brightness.get()
			self.imageGR.contrast = self.GR_contrast.get()
			self.imageGR.makeGR_Image()
			
			self.GR_Final = self.imageGR.imageTest
			if self.GR_FinalThumbnailX2:
				self.GR_Final = self.GR_Final.resize((self.appleScr[0]*2,(self.appleScr[1]-32)*2))
				self.canGR_Final.configure(width=self.appleScr[0]*2, height=(self.appleScr[1]-32)*2)
				
			self.canGR_FinalImage = ImageTk.PhotoImage(self.GR_Final)
			self.canGR_Final.create_image(3,3, anchor=NW, image=self.canGR_FinalImage)
			self.canGR_Final.configure(background="black")
			self.fieldGR.delete(0.1, END)
			self.fieldGR.insert(INSERT, self.imageGR.listing)
			self.copyHexaToClipboard(data=self.imageGR.listing, dataType="basic low res")
			return 1
		else:
			return 0
		
	def GR_basicCopyClipboard(self, event=None):
		self.copyHexaToClipboard(data=self.imageGR.listing, dataType="basic low res")
		self.winDone("basic code copied to clipboard")
		return "break"
		
	def openGR_Image(self, event=None):
		fileName = tkDia.askopenfilename(parent=self.winGR, title="Open Image File", initialdir=self.openDiaRoot, filetypes=[("Image",("*.png","*.jpg","*.jpeg","*.gif")), ("all files", ".*")])
		if fileName:
			self.openDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			# set original image
			try:
				self.originalGR_Image = Image.open(fileName).resize(self.appleScr)
			except:
				self.winErrorFile("can't open "+fileName)
				return 0
				
			# set image values with new imag
			self.winGR.title("Apple Low Res Image Editor : "+fileName)
			self.imageGR = ImgToAppleGR()
			self.refreshGR_Image()
		else:
			self.winCancel("open image cancelled")
			return 0
			
		return 1
		
	def saveGR_Basic(self, event=None):
		fileName = tkDia.asksaveasfilename(
			parent=self.tkRoot, title="Save AppleSoft Basic code", initialdir=self.saveDiaRoot, 
			initialfile="" , filetypes=[("Basic bas","*.bas")]
		)
		if fileName and self.imageGR:
			self.saveDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			try:
				f = open(self.addExtension(fileName=fileName, ext="bas"),"w")
				f.write(self.imageGR.listing)
				f.close()
				im = self.imageGR.imageTest.resize((1120,640))
				im.save(self.addExtension(fileName=fileName, ext="png"),"PNG")
			except:
				self.winErrorFile("can't save Basic and image")
				return 0
		else:
			self.winCancel("save Basic and image cancelled")
			return 0
		self.winDone(fileName+" saved")
		return 1

	def saveGR_image(self, event=None):
		fileName = tkDia.asksaveasfilename(
			parent=self.tkRoot, title="Save Picture Image PNG", initialdir=self.saveDiaRoot, 
			initialfile="img-low-res-"+strftime("%Y-%m-%d-%H-%M-%S", localtime())+".png" , filetypes=[("Image png","*.png")]
		)
		if fileName and self.GR_Final:
			self.saveDiaRoot = path.dirname(fileName) # set new root directory for ask dialog
			self.storeVars()
			
			# save apple screen img as png
			w,h = self.GR_Final.size[0]*4,self.GR_Final.size[1]*4
			im = self.GR_Final.resize((w,h)) # png is 4 x bigger
			fileName = self.addExtension(fileName=fileName, ext="png")
			if not self.savePng(im=im, fileName=fileName):return self.winErrorFile("can't write "+fileName)
		else:
			self.winCancel("save low res picture cancelled")
			return 0
			
		return self.winDone(fileName+" saved")	
			
	# -------------------------------- inspect vars tool
	def openInspectVars(self, event=None):
		try:
			self.winInspectVars.destroy()
		except:
			pass
			
		self.winInspectVars = Toplevel(self.tkRoot)	
		self.winInspectVars.withdraw() # hide win	
		self.winInspectVars.protocol("WM_DELETE_WINDOW", self.closeWinInspectVars)
		self.winInspectVars.title("Inpect Vars")
		try: # windows only
			self.winInspectVars.attributes("-toolwindow", 1)
		except:
			pass
		
		self.fieldVars = Text(self.winInspectVars, font=self.customFont, width=120, height=42, relief=SUNKEN, wrap=WORD, undo=True, bg="black", fg="green1")
		scrollbarVars = Scrollbar(self.winInspectVars)
		scrollbarVars.configure(command=self.fieldVars.yview)
		self.fieldVars.config(yscrollcommand=scrollbarVars.set)
		
		varsButtonsFrame = Frame(self.winInspectVars)
		appleImageVarsButton = Button(varsButtonsFrame, font=self.customFontBold, text="ImgToAppleHGR", command=self.getInspectVarsAppleImage)
		editorVarsButton = Button(varsButtonsFrame, font=self.customFontBold, text="Editor", command=self.getInspectVarsEditor)
		imageVarsButton = Button(varsButtonsFrame, font=self.customFontBold, text="Image", command=self.getEditorVars)
		appleBinDataButton = Button(varsButtonsFrame, font=self.customFontBold, text="AppleBinData", command=self.getAppleBinDataVar)
		appleBinDataFinalButton = Button(varsButtonsFrame, font=self.customFontBold, text="AppleBinData Final", command=self.getAppleBinDataFinalVar)
		applePixelMapButton = Button(varsButtonsFrame, font=self.customFontBold, text="Pixel Map", command=self.getPixelMapVar)
		appleColorMapButton = Button(varsButtonsFrame, font=self.customFontBold, text="Color Map", command=self.getColorMapVar)
		
		# grid
		inspectVarsGrid = [
			# widget							r  c  rs cs sticky
			[varsButtonsFrame, 					0, 0, 1, 3, N+E],
			[self.fieldVars, 					1, 0, 1, 2, N+W+E+S],
			[scrollbarVars, 					1, 3, 1, 1, N+S]
		]
		for w in inspectVarsGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
			
		# grid
		varsButtonsFrameGrid = [
			# widget							r  c  rs cs sticky
			[appleImageVarsButton, 				0, 0, 1, 1, W],
			[editorVarsButton, 					0, 1, 1, 1, W],
			[imageVarsButton, 					0, 2, 1, 1, W],
			[appleBinDataButton, 				0, 3, 1, 1, W],
			[appleBinDataFinalButton, 			0, 4, 1, 1, W],
			[applePixelMapButton, 				0, 5, 1, 1, W],
			[appleColorMapButton, 				0, 6, 1, 1, W]
		]
		for w in varsButtonsFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
			
		# make entries resizable
		self.winInspectVars.grid_columnconfigure(1, weight=1)
		self.winInspectVars.grid_rowconfigure(1, weight=1)
		
		if self.winInspectVarsPos:self.winInspectVars.geometry(self.winInspectVarsPos)
		self.getInspectVarsEditor()
		self.winInspectVars.deiconify() # show win
		self.winInspectVars.mainloop()
	
	def getAppleBinDataFinalVar(self, event=None, max=3000):
		self.fieldVars.delete(0.1, END)
		self.fieldVars.insert(INSERT, self.printConTxt(t="AppleBinData in final order data")+"\n")
		vars = self.aIeCreateAppleBin(self.appleImage.appleBinData)
		self.fieldVars.insert(INSERT, vars[0:max]+[" ... only first "+str(max)])
		
	def getAppleBinDataVar(self, event=None, max=3000):
		self.fieldVars.delete(0.1, END)
		self.fieldVars.insert(INSERT, self.printConTxt(t="AppleBinData data")+"\n")
		self.fieldVars.insert(INSERT, self.appleImage.appleBinData[0:max]+[" ... only first "+str(max)])
		
	def getPixelMapVar(self, event=None, max=3000):
		self.fieldVars.delete(0.1, END)
		self.fieldVars.insert(INSERT, self.printConTxt(t="Pixel Map data")+"\n")
		self.fieldVars.insert(INSERT, self.appleImage.pixelMap[0:max]+[" ... only first "+str(max)])
		
	def getColorMapVar(self, event=None, max=1000):
		self.fieldVars.delete(0.1, END)
		self.fieldVars.insert(INSERT, self.printConTxt(t="Color Map data")+"\n")
		self.fieldVars.insert(INSERT, self.appleImage.appleBinColorMap[0:max]+[" ... only first "+str(max)])
		
	def getEditorVars(self, event=None):
		valNames = ["bit_MSB", "random_MSB", "dither", "brightness", "contrast", "effect", "shiftRight",
		"shiftDown", "geometry", "warpSoft", "warp", "zigZagWarp", "noise", "randNoise", 
		"bitWise", "imageType", "linesBetween", "linesByteVal", "lineStep", "imageMarkContent"]
		params = self.getEditorParams()
		result = ""
		for i in xrange(0,len(params)):
			result += valNames[i]+" _ "+str(params[i])+"\n"
			
		result += "len "+str(len(params))
		self.fieldVars.delete(0.1, END)
		self.fieldVars.insert(INSERT, self.printConTxt(t="Editor Image Vars")+"\n")
		self.fieldVars.insert(INSERT, result)
		
	def getEditorVarsText(self):
		params = self.getEditorParams()
		txt = "["
		for i in xrange(0,len(params)):
			if type(params[i]).__name__ == "int":
				txt += str(params[i])+","
			else:
				txt += "'"+params[i]+"',"
				
		txt = txt[:-1]+"]"
		return txt
	
	def getInspectVarsAppleImage(self, event=None):
		badNames = r'label|can|field|Button|Frame|custom|mBuffer|mEdit|tkRoot|blue|orange|violet|green|white|black'
		varsFilter = re.compile(badNames)
		varsText = []
		varsText.append(self.printConTxt(t="Inspect vars"))
		varsText.append("Vars masked :["+", ".join(badNames.split("|"))+"]")
		
		# imgToAppleHGR	vars
		varsText.append(" ")
		varsText.append(self.printConTxt(t="Apple Image vars"))
		for name in sorted(vars(self.appleImage)):
			if not varsFilter.search(name):
				done = False
				try:
					e = eval('self.appleImage.'+name)
					done = True
				except:
					done = False
					
				if done:
					if type(e).__name__ == "list":
						varsText.append("self.appleImage."+name+" _ ["+self.printConList(l=e)+"] _ len "+str(len(e)))
					elif type(e).__name__ == "tuple":
						varsText.append("self.appleImage."+name+" _ ("+self.printConList(l=e)+") _ len "+str(len(e)))
					elif type(e).__name__ == "dict":
						varsText.append("self.appleImage."+name+" _ {"+self.printConList(l=e.keys())+"}")
					else:
						varsText.append("self.appleImage."+name+" _ "+str(e)+" _ "+type(e).__name__)
						
		self.fieldVars.delete(0.1, END)
		self.fieldVars.insert(INSERT, "\n".join(varsText))
						
	def getInspectVarsEditor(self, event=None):
		badNames = r'label|can|field|Button|Frame|custom|mBuffer|mEdit|tkRoot|blue|orange|violet|green|white|black'
		varsFilter = re.compile(badNames)
		varsText = []
		varsText.append(self.printConTxt(t="Inspect vars"))
		varsText.append("Vars masked :["+", ".join(badNames.split("|"))+"]")
		
		# appleImageEditor vars
		varsText.append(" ")			
		varsText.append(self.printConTxt(t="Editor vars"))
		for name in sorted(vars(self)):
			if not varsFilter.search(name):
				done = False
				try:
					e = eval('self.'+name+'.get()')
					done = True 
				except:
					done = False
					
				if not done:
					try:
						e = eval('self.'+name)
						done = True
					except:
						done = False
				if done:
					if type(e).__name__ == "list":
						varsText.append("self."+name+" _ ["+self.printConList(l=e)+"] _ len "+str(len(e)))
					elif type(e).__name__ == "tuple":
						varsText.append("self."+name+" _ ("+self.printConList(l=e)+") _ len "+str(len(e)))
					elif type(e).__name__ == "dict":
						varsText.append("self."+name+" _ {"+self.printConList(l=e.keys())+"}")
					else:
						varsText.append("self."+name+" _ "+str(e)+" _ "+type(e).__name__)
						
		self.fieldVars.delete(0.1, END)
		self.fieldVars.insert(INSERT, "\n".join(varsText))
		
	def closeWinInspectVars(self, event=None):
		self.winInspectVarsPos = self.winInspectVars.geometry()
		self.winInspectVars.destroy()
	
	def printConList(self, l=None, s=10):
		arr = []
		if len(l) > s:
			for e in l[0:s]: # avoid 2d arrays
				if type(e).__name__ == "list" or type(e).__name__ == "tuple":
					arr.append(e[0])
				else:
					arr.append(e)
			return ", ".join(map(str,arr))
		elif l:
			for e in l:
				if type(e).__name__ == "list" or type(e).__name__ == "tuple":
					arr.append(e[0])
				else:
					arr.append(e)
			return ", ".join(map(str,arr))
		else:
			return ""
			
	def printConTxt(self, t="", s=80, c="#"):
		s -= 1
		if len(t) < s:
			return t+" "+c*(s-len(t))
		else:
			return t
		
	# -------------------------------- hexadecimal/binary converter
	def openConverter(self, event=None):
		try:
			self.winConverter.destroy()
		except:
			pass
			
		self.winConverter = Toplevel(self.tkRoot)
		self.winConverter.withdraw() # hide win
		self.winConverter.protocol("WM_DELETE_WINDOW", self.closeWinConverter)
		self.winConverter.title("Converter")
		try: # windows only
			self.winConverter.attributes("-toolwindow", 1)
		except:
			pass
		
		labelConverter = Label(self.winConverter, text="Decimal/Hexadecimal/Binary Converter", font=self.customFontBold)
		# labels
		labelDecimal = Label(self.winConverter, text="Dec", font=self.customFontBold)
		labelHexadecimal = Label(self.winConverter, text="Hex", font=self.customFontBold)
		labelBinary = Label(self.winConverter, text="Bin", font=self.customFontBold)
		# entries
		self.eDecimal = Entry(self.winConverter, textvariable=self.convertDecimalVal, width=40, font=self.customFont, relief=SUNKEN)
		if self.selectedInField: self.convertHexVal.set(self.selectedInField) # put selected text into entry
		self.eHexadecimal = Entry(self.winConverter, textvariable=self.convertHexVal, width=40, font=self.customFont, relief=SUNKEN)
		self.eBinary = Entry(self.winConverter, textvariable=self.convertBinaryVal, width=40, font=self.customFont, relief=SUNKEN)
		# don't forget to compute hexa if selected text in field after entries have been created
		if self.selectedInField:self.convertHexValCaps()
		# binds
		self.eDecimal.bind("<KeyRelease>", lambda event:self.convertDecHexBin(base=10))
		self.eHexadecimal.bind("<KeyRelease>", lambda event:self.convertDecHexBin(base=16))
		self.eBinary.bind("<KeyRelease>", lambda event:self.convertDecHexBin(base=2))
		
		# grid
		converterGrid = [
			# widget							r  c  rs cs sticky
			[labelConverter, 					0, 0, 1, 2, N+W],
			[labelDecimal, 						1, 0, 1, 1, N+W+E],
			[self.eDecimal, 					1, 1, 1, 1, N+W+E],
			[labelHexadecimal,					2, 0, 1, 1, N+W+E],
			[self.eHexadecimal, 				2, 1, 1, 1, N+W+E],
			[labelBinary, 						3, 0, 1, 1, N+W+E],
			[self.eBinary, 						3, 1, 1, 1, N+W+E]
		]
		for w in converterGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
		
		self.eDecimal.focus_set()
		
		# make entries resizable
		self.winConverter.grid_columnconfigure(1, weight=1)
		
		if self.winConverterPos:self.winConverter.geometry(self.winConverterPos)
		self.winConverter.deiconify() # show win
		self.winConverter.mainloop()
			
	def convertDecHexBin(self, event=None, base=0):
		self.convertHexVal.set(self.eHexadecimal.get().upper()) # set hexadecimal to caps
		if self.eDecimal.get() and base == 10:
			try:
				decValues = map(int, self.eDecimal.get().split())
				
				hewValues = [hex(x)[2:].zfill(2).upper() for x in decValues]
				result = " ".join(map(str,hewValues))
				self.convertHexVal.set(result)
				
				binValues = [bin(x)[2:].zfill(8) for x in decValues]
				result = " ".join(map(str,binValues))
				self.convertBinaryVal.set(result)
			except:
				result = "Error !"
				self.convertHexVal.set(result)
				self.convertBinaryVal.set(result)
		elif self.eHexadecimal.get() and base == 16:
			try:
				decValues = map(lambda x: int(x, 16), self.eHexadecimal.get().split())
				result = " ".join(map(str,decValues))
				self.convertDecimalVal.set(result)
				
				binValues = [bin(x)[2:].zfill(8) for x in decValues]
				result = " ".join(map(str,binValues))
				self.convertBinaryVal.set(result)
			except:
				result = "Error !"
				self.convertDecimalVal.set(result)
				self.convertBinaryVal.set(result)
		elif self.eBinary.get() and base == 2:
			try:
				decValues = map(lambda x: int(x, 2), self.eBinary.get().split())
				result = " ".join(map(str,decValues))
				self.convertDecimalVal.set(result)
				
				hewValues = [hex(x)[2:].zfill(2).upper() for x in decValues]
				result = " ".join(map(str,hewValues))
				self.convertHexVal.set(result)
			except:
				result = "Error !"
				self.convertHexVal.set(result)
				self.convertBinaryVal.set(result)
		
	def convertHexBinReset(self, event=None):
		self.convertDecimalVal.set("")
		self.convertHexVal.set("")
		self.convertBinaryVal.set("")
	
	def closeWinConverter(self, event=None):
		self.winConverterPos = self.winConverter.geometry()
		self.winConverter.destroy()
		
	def convertHexValCaps(self, event=None):
		self.convertHexVal.set(self.eHexadecimal.get().upper())
		self.convertDecHexBin(base=16)

	# -------------------------------- bitWise
	def openBitWise(self, event=None):
		try:
			self.winBitWise.destroy()
		except:
			pass
		
		self.winBitWise = Toplevel(self.tkRoot)	
		self.winBitWise.protocol("WM_DELETE_WINDOW", self.closeWinBitWise)
		self.winBitWise.title("Bitwise operator")
		try: # windows only
			self.winBitWise.attributes("-toolwindow", 1)
		except:
			pass
		
		labelBit = Label(self.winBitWise, text="Click on operator or bit to set value", font=self.customFontBold)
		
		# bitwize : 1^1 = 0, 1^0 = 1, 0^0 = 0, 1|1 = 1, 1|0 = 1, 0|0 = 0, 1&1 = 1, 1&0 = 0, 0&0 = 0
		labelOperator = Label(self.winBitWise, font=self.customFont, text="Op")
		labels = [None]*7
		for i in xrange(0,7):
			labels[i] = Label(self.winBitWise, font=self.customFont, text=str(6-i))
			
		# bit buttons
		fontStyle = self.customFontBold
		self.bitWiseOperatorButton = Button(self.winBitWise, font=fontStyle, text=self.bitWiseOperator, command=self.bitWiseOperatorState)
		
		for i in xrange(0,7): # in loop for i... lambda need his own val (x) instead of i
			self.bitButton[i] = Button(self.winBitWise, font=fontStyle, text=self.bits[i], command=lambda x=i:self.bitState(bit=x))
		
		bitWiseInverseButton = Button(self.winBitWise, font=self.customFontBold, text="Inverse", command=self.inverseBitWise)
		bitWiseResetButton = Button(self.winBitWise, font=self.customFontBold, text="Reset", command=self.resetBitWise)
		
		# grid
		labelBit.grid(row=0, column=0, columnspan=8, sticky=N+W)
		
		labelOperator.grid(row=1, column=0, sticky=N+W+E)
		for i in xrange(0,7):
			labels[i].grid(row=1, column=i+1, sticky=N+W+E)
			
		self.bitWiseOperatorButton.grid(row=2, column=0, sticky=N+W+E)
		for i in xrange(0,7):
			self.bitButton[i].grid(row=2, column=i+1, sticky=N+W+E)
			
		bitWiseInverseButton.grid(row=3, column=0, columnspan=4, sticky=N+W+E)	
		bitWiseResetButton.grid(row=3, column=4, columnspan=4, sticky=N+W+E)
		
		for col in xrange(0,8):# set resize to all columns 
			self.winBitWise.grid_columnconfigure(col, weight=1)
			
		if not self.winBitWisePos: # set position of bitWiseWin
			self.winBitWise.update() # update win
			wRoot = []; wBitBtn = []; wBitWise = [] # geometry values of elements
			[wRoot.extend(x) for x in [x.split("+") for x in [x for x in self.tkRoot.geometry().split("x")]]]
			[wBitBtn.extend(x) for x in [x.split("+") for x in [x for x in self.bitWiseButton.winfo_geometry().split("x")]]]
			[wBitWise.extend(x) for x in [x.split("+") for x in [x for x in self.winBitWise.geometry().split("x")]]]
			
			firstTime = wBitWise[0]+"x"+wBitWise[1]+"+"+str(int(wRoot[2])+int(wBitBtn[2])+5)+"+"+str(int(wRoot[3])+int(wBitBtn[3])+int(wBitWise[1])+5)
			self.winBitWisePos = firstTime
		
		self.winBitWise.geometry(self.winBitWisePos)
		self.winBitWise.mainloop()
	
	def updateBitWise(self, bitWise=None): # only values not in winBitWise
		for i in xrange(0,7):
			self.bits[i] = int(bitWise[i+1])
			
		self.bitWiseOperator = bitWise[0]
		self.bitWise = bitWise[0]+"".join(map(str, self.bits))
		self.bitWiseButton.configure(text=self.bitWise)
			 
	def inverseBitWise(self):
		for i in xrange(0,7):
			if self.bits[i] == 0:
				self.bits[i] = 1
			else:
				self.bits[i] = 0
				
		for i in xrange(0,7):
			self.bitButton[i].configure(text=str(self.bits[i]))
			
		self.bitWise = self.bitWiseOperator+"".join(map(str, self.bits))
		self.bitWiseOperatorButton.configure(text=self.bitWiseOperator)
		self.bitWiseButton.configure(text=self.bitWise)
		self.refreshAppleImage()
		
	def closeWinBitWise(self, event=None):
		self.winBitWisePos = self.winBitWise.geometry()
		self.winBitWise.destroy()
		
	def resetBitWise(self):
		self.bitWise = "^0000000" # reset bitwise
		self.bitWiseButton.configure(text=self.bitWise)
		self.bits = [0]*7 # reset bits
		for i in xrange(0,7):
			self.bitButton[i].configure(text=str(self.bits[i]))
			
		self.bitWiseOperator = "^" # reset operator
		self.bitWiseOperatorButton.configure(text=self.bitWiseOperator)
		self.refreshAppleImage()
	
	def bitWiseOperatorState(self, event=None):
		if self.bitWiseOperator == "^":
			self.bitWiseOperator = "|"
		elif self.bitWiseOperator == "|":
			self.bitWiseOperator = "&"
		else:
			self.bitWiseOperator = "^"
			
		self.bitWiseOperatorButton.configure(text=self.bitWiseOperator)
		self.bitWise = self.bitWiseOperator+"".join(map(str, self.bits))
		self.bitWiseButton.configure(text=self.bitWise)
		self.refreshAppleImage()
		
	def bitState(self, event=None, bit=0):
		if self.bits[bit] == 0:
			self.bits[bit] = 1
		else:
			self.bits[bit] = 0
		
		self.bitButton[bit].configure(text=str(self.bits[bit]))
		self.bitWise = self.bitWiseOperator+"".join(map(str, self.bits))
		self.bitWiseButton.configure(text=self.bitWise)
		self.refreshAppleImage()
	
	# -------------------------------- find replace
	def findAndReplaceWin(self, event=None):
		try:
			self.findReplaceWin.destroy()
		except:
			pass
			
		# draw the find/replace dialog
		self.findReplaceWin = Toplevel(self.tkRoot)
		self.findReplaceWin.withdraw()
		self.findReplaceWin.title("Find/Replace Ctrl+N next")
		self.findReplaceWin.protocol("WM_DELETE_WINDOW", self.quitFindReplaceWin)
		try: # windows only
			self.findReplaceWin.attributes("-toolwindow", 1)
		except:
			pass
				
		self.fieldCodeHexa.tag_config("find", foreground="yellow", background="dark blue")
		
		# top frame
		topFrame = Frame(self.findReplaceWin)
		
		# label find and entry word
		findWord = Label(topFrame, text="Find", font=self.customFont)
		self.eFind = Entry(topFrame, textvariable=self.searchText, width=40, font=self.customFont, relief=SUNKEN)
		self.eFind.bind("<KeyRelease>", lambda event:self.upperCase(w=self.searchText, r=self.regexStatus.get()))	
		if self.selectedInField: self.searchText.set(self.selectedInField)
		 
		# label replace and entry word
		replaceWord = Label(topFrame, text="Replace", font=self.customFont)
		self.eReplace = Entry(topFrame, textvariable=self.replaceText, width=40, font=self.customFont, relief=SUNKEN)
		self.eReplace.bind("<KeyRelease>", lambda event:self.upperCase(w=self.replaceText))	
		
		# buttons
		buttonFindNext = Button(topFrame, text="Find Next", font=self.customFont, command=self.doFindText)
		buttonFindReplace = Button(topFrame, text="Replace", font=self.customFont, command=self.doReplaceText)
		buttonFindReplaceAll = Button(topFrame, text="Replace All", font=self.customFont, command=self.doReplaceTextAll)
		buttonCancel = Button(topFrame, text="Cancel", font=self.customFont, command=self.quitFindReplaceWin)

		# bottom frame
		bottomFrame = Frame(self.findReplaceWin)
		
		# regex 
		self.regexButton = Checkbutton(bottomFrame, text="RegExp", font=self.customFont, variable=self.regexStatus)
		if self.regexStatus.get(): self.regexButton.select()
		
		# status
		self.findReplaceStatus = Label(bottomFrame, text="status")
		
		# grid
		topFrameGrid = [
			# widget							r  c  rs cs sticky
			[findWord, 							0, 0, 1, 1, W],
			[self.eFind, 						0, 1, 1, 1, E],
			[replaceWord, 						1, 0, 1, 1, W],
			[self.eReplace,						1, 1, 1, 1, E],
			[buttonFindNext, 					0, 2, 1, 1, W+E],
			[buttonFindReplace, 				1, 2, 1, 1, W+E],
			[buttonFindReplaceAll, 				2, 1, 1, 1, W],
			[buttonCancel, 						2, 2, 1, 1, W+E]
		]
		topFrame.grid(row=0, column=0)
		for w in topFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
		
		bottomFrameGrid = [
			[self.regexButton, 					0, 0, 1, 1, W],
			[self.findReplaceStatus, 			1, 0, 1, 3, W]
		]
		bottomFrame.grid(row=1, column=0, sticky=E+W)
		for w in bottomFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
		
		self.eFind.focus_set()
		# shorcuts
		self.findReplaceWin.bind("<Return>",self.doFindText)
		self.findReplaceWin.bind("<Control-n>",self.doFindText)
		
		if self.winFindPos:self.findReplaceWin.geometry(self.winFindPos)
		
		self.findReplaceWin.deiconify()# show win
		self.findReplaceWin.mainloop()
		
	def quitFindReplaceWin(self, event=None):
		self.winFindPos = self.findReplaceWin.geometry()
		self.findReplaceWin.destroy()
		
	def doFindText(self, event=None):			
		field = self.fieldCodeHexa
		field.tag_remove("find", 1.0, END) # reset found text tag	
		lenSearch = StringVar()
		pos = field.search(self.searchText.get(), self.nextFind, stopindex="end", nocase=1, regexp=self.regexStatus.get(), count=lenSearch)
		if not pos:
			self.findTextPos = None
			self.nextFind = 1.0
			self.findReplaceStatus.configure(text="not found or end of text", foreground="red")
			return 0
	
		line, col = pos.split('.')
		self.findTextPos = (pos,"%s.%d"%(line, int(col)+int(lenSearch.get()))) # remenber find pos and length
		self.nextFind = pos + "+"+lenSearch.get()+"c"
		
		field.tag_add("find", pos, "%s.%d"%(line, int(col)+int(lenSearch.get())))
		field.see(pos) # need to scroll to found text
		
		self.findReplaceStatus.configure(text=self.searchText.get()+" found at ln:"+line+" ch:"+col, foreground="black")
		
	def doReplaceText(self, event=None):
		field = self.fieldCodeHexa# get entry replace
		if self.findTextPos:# check if find pos is ok
			field.delete(self.findTextPos[0],self.findTextPos[1])# delete old text
			field.insert(self.findTextPos[0],self.replaceText.get())# insert replace text
			self.nextFind = self.findTextPos[0] + "+"+str(len(self.replaceText.get()))+"c" # update next find pos with new length
		else:
			self.findReplaceStatus.configure(text="no find to replace")
			
	def doReplaceTextAll(self, event=None):
		field = self.fieldCodeHexa
		replaceTimes = 0 # set counter
	
		if self.eFind.get():
			field.tag_remove("find", 1.0, END)# reset found text tag
			start = 1.0
			while 1:# parse all text
				lenSearch = StringVar()
				pos = field.search(self.searchText.get(), start, stopindex="end", nocase=1, regexp=self.regexStatus.get(), count=lenSearch)
				if not pos:break # stop do find/replace
				field.see(pos) # need to scroll to found text
				
				line, col = pos.split('.')
				findTextPos = (pos,"%s.%d"%(line, int(col)+int(lenSearch.get())))
				field.delete(findTextPos[0],findTextPos[1])# delete old text
				field.insert(findTextPos[0],self.replaceText.get())# insert replace text
				start = pos + "+"+str(len(self.replaceText.get()))+"c"
				replaceTimes += 1
			
			self.findReplaceStatus.configure(text=self.searchText.get()+" found/replaced "+str(replaceTimes)+" times", foreground="black")
		else:
			self.findReplaceStatus.configure(text="nothing to replace")
			
	def getSelection(self, event):# get selected text	
		try:# if no selection get tcl error message
			self.selectedInField = self.fieldCodeHexa.selection_get()
			self.nextFind = self.fieldCodeHexa.index("sel.last")
		except:# reset find
			self.selectedInField = ""
			self.nextFind = 1.0
	
	# -------------------------------- draw interface
	def drawInterface(self):
		# set menu
		menubar = Menu(self.tkRoot)
		# file
		mFile = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="File", menu=mFile)
		mFile.add_command(label="Open Source Image File", command=self.openImageSource, accelerator="Ctrl+O")
		mFile.add_command(label="Open Apple Bin Image File", command=self.openAppleBinaryFile, accelerator="Ctrl+Alt-O")
		mFile.add_command(label="Open Hexadecimal Text File", command=self.openHexaFileListing)
		mFile.add_separator()
		mFile.add_command(label="Save Image Png+Bin+Hexa+Data", command=self.saveAll, accelerator="Ctrl+S")
		mFile.add_command(label="Save Apple Bin File", command=self.saveImageBin, accelerator="Ctrl+Alt-S")
		mFile.add_command(label="Save Hexadecimal Code", command=self.saveImageHex, accelerator="Ctrl+H")
		mFile.add_separator()
		mFile.add_command(label="Save Full Working Data (images, bin, hexa, data)", command=self.saveFullData, accelerator="Button-3")
		mFile.add_command(label="Save Only Pixels Map To Png", command=self.savePixelMapToPng, accelerator="Button-1")
		mFile.add_command(label="Export Image to Tiff", command=self.exportTiff)
		mFile.add_separator()
		mFile.add_command(label="Quit", command=self.tkRoot.destroy, accelerator="Ctrl+Q")
		
		# edit
		self.mEdit = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Edit", menu=self.mEdit)
		self.mEdit.add_command(label="Copy Hex Code", command=self.copyHexaToClipboard)
		self.mEdit.add_command(label="Refresh Image", command=self.refreshAppleImage, accelerator="Ctrl+R")
		self.mEdit.add_command(label="Reset To Original Image", command=self.resetOriginal)
		self.mEdit.add_separator()
		self.mEdit.add_command(label="Show Color Map", command=self.showColorMap)
		self.mEdit.add_separator()
		self.mEdit.add_command(label="Bitwise Operator", command=self.openBitWise, accelerator="Ctrl+B")
		self.mEdit.add_separator()
		self.mEdit.add_command(label="Undo Last Hexadecimal Change", command=self.undoBack)
		
		# tools
		self.mTools = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Tools", menu=self.mTools)
		self.mTools.add_command(label="Find/Replace", command=self.findAndReplaceWin, accelerator="Ctrl+F")
		self.mTools.add_command(label="Dec/Hex/Bin Converter", command=self.openConverter)
		self.mTools.add_command(label="Low Res Image Editor", command=self.openGR_Editor)
		self.mTools.add_command(label="Animation Workshop", command=self.openAnimation)
		if path.isfile(gimp): self.mTools.add_command(label="Open Image source with GIMP", command=self.openImageGimp)
		if path.isfile(appleWin): self.mTools.add_command(label="Open AppleWin", command=self.openAppleWin)
		self.mTools.add_separator()
		self.mTools.add_command(label="Copy Complete Listing to Clipboard", command=self.doCompleteListing)
		self.mTools.add_command(label="Use Example Test Pattern", command=self.testPattern)
		self.mTools.add_separator()
		self.mTools.add_command(label="Inspect Editor Vars", command=self.openInspectVars)
		
		# dat params
		self.mParamsBuffers = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Parameters", menu=self.mParamsBuffers)
		self.mParamsBuffers.add_command(label="Load Parameters From Dat File ", command=self.openParams)
		self.mParamsBuffers.add_command(label="Save Parameters To Dat File", command=self.saveParamsToFile)
		self.mParamsBuffers.add_command(label="Delete active Parameters", command=self.deleteParamsBufferEntry)
		self.mParamsBuffers.add_separator()
		
		# zoom
		mZoom = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Zoom", menu=mZoom)
		mZoom.add_command(label="Zoom Original Image", command=self.zoomImageOriginal, accelerator="Button-1")
		mZoom.add_command(label="Zoom Pixel Map Image", command=self.zoomPixelMap, accelerator="Button-1")
		mZoom.add_command(label="Zoom Apple Screen Image", command=self.zoomImageResult, accelerator="Button-1")
		mZoom.add_separator()
		mZoom.add_command(label="Zoom Apple Screen Thumbnail + / -", command=self.appleScreenImageBig, accelerator="Button-2")
		mZoom.add_separator()
		mZoom.add_command(label="Zoom Hexadecimal Text +", command=self.zoomTextIn, accelerator="Ctrl+Up")
		mZoom.add_command(label="Zoom Hexadecimal Text -", command=self.zoomTextOut, accelerator="Ctrl+Down")
		
		# help
		mHelp = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Help", menu=mHelp)
		mHelp.add_command(label="Help Open Binary or Hexadecimal", command=self.helpOpenBinHexa)
		mHelp.add_command(label="Help Hexadecimal $2000:$3FFF", command=self.helpHexadecimal)
		mHelp.add_command(label="Help Raw Hexadecimal", command=self.helpHexadecimalRaw)
		mHelp.add_command(label="Help Add New Buffer", command=self.helpAddNewBuffer)
		mHelp.add_command(label="Help Reset Button", command=self.helpResetButton)
		mHelp.add_command(label="Help Refresh Button", command=self.helpRefreshButton)
		
		# buffers
		self.mBuffers = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Buffers", menu=self.mBuffers)
		self.mBuffers.add_command(label="Add Current Work To Buffers", command=self.newImageSource, accelerator="Ctrl+N")
		self.mBuffers.add_command(label="Delete active buffer", command=self.deleteBufferEntry)
		self.mBuffers.add_separator()
		
		# about
		mAbout = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="About", menu=mAbout)
		mAbout.add_command(label="Author", command=self.about)
		mAbout.add_command(label="Apple Image Editor", command=self.appleImageEditorInfos)
		
		# set menu
		self.tkRoot.configure(menu=menubar)

		# set shortcuts for menu
		mainShortcuts = {
			"<Control-q>":		self.quitInterface,
			"<Control-o>":		self.openImageSource,
			"<Control-Alt-o>":	self.openAppleBinaryFile,
			"<Control-s>":		self.saveAll,
			"<Control-Alt-s>":	self.saveImageBin,
			"<Control-h>":		self.saveImageHex,
			"<Control-r>":		self.refreshAppleImageFx,
			"<Control-n>":		self.newImageSource,
			"<Control-b>":		self.openBitWise,
			"<Control-f>":		self.findAndReplaceWin
		}
		for event, handler in mainShortcuts.items():
			self.tkRoot.bind_all(event, handler)
			
		# GUI, frames, widgets, grid
		rootFrame = self.tkRoot
		self.warpFrame = Frame(rootFrame)
		self.appleScreenFrame = Frame(rootFrame, borderwidth=3, relief=GROOVE)
		# ============================ help
		self.labelHelp = Label(rootFrame, font=self.customFontBold, justify=LEFT, text="Click on Thumbails to zoom\nParams in left white zone\nare disabled if hexadecimal modified\nParams in right white zone\ndon't concern apple bin image", fg="red")
		
		# ============================ original and source frames
		self.labelOriginal = Label(rootFrame, font=self.customFontBold, text="Original Image")
		self.canOriginal = Canvas(rootFrame, width=self.appleScr[0], height=self.appleScr[1], cursor="hand1", borderwidth=1, background="black")
		self.canOriginal.bind("<Button-1>",self.zoomImageOriginal)
		
		self.labelPixelMap = Label(rootFrame, font=self.customFontBold, text="Image Pixels Map")
		self.canPixelMap = Canvas(rootFrame, width=self.appleScr[0], height=self.appleScr[1], cursor="hand1", borderwidth=1, background="black")
		self.canPixelMap.bind("<Button-1>",self.zoomPixelMap)
		self.canPixelMap.bind("<Button-3>",self.savePixelMapToPng)
		
		# ============================ primary parameters
		# ============================ apple screen frame
		self.imageType = StringVar(); self.imageType.set("HGR")
		imageTypeOptionMenu = OptionMenu(self.appleScreenFrame, self.imageType, "HGR", "HGR adaptative", "Black/White", "Adaptative", "WEB palette", command=self.refreshAppleImage)
		imageTypeOptionMenu.configure(font=self.customFont)
		
		self.dither = IntVar()
		self.ditherButton = Checkbutton(self.appleScreenFrame, text="Dither", font=self.customFont, variable=self.dither)
		self.ditherButton.select()
		
		self.brightness = Scale(self.appleScreenFrame, from_=0, to=100, showvalue=0, label="Brightness", font=self.customFont, orient=HORIZONTAL)
		self.brightness.set(70)

		self.contrast = Scale(self.appleScreenFrame, from_=0, to=100, showvalue=0, label="Contrast", font=self.customFont, orient=HORIZONTAL)
		self.contrast.set(70)
		# ============================ end apple screen frame
		
		self.shiftRight = Scale(rootFrame, from_=0, to=100, showvalue=0, label="Right", font=self.customFont, orient=HORIZONTAL)
		self.shiftRight.set(0)
		
		self.shiftDown = Scale(rootFrame, from_=0, to=100, showvalue=0, label="Down", font=self.customFont, orient=VERTICAL)
		self.shiftDown.set(0)
		
		self.geometry = StringVar(); self.geometry.set("GeometryOff")
		self.geometryOptionMenu = OptionMenu(rootFrame, self.geometry, 
			"GeometryOff", "UpsideDown", "Flip", "Down+Flip", "Mirror H", 
			"Mirror H inv", "Mirror V", "Mirror V inv", "Mirror H+V", command=self.refreshAppleImage
		)
		self.geometryOptionMenu.configure(font=self.customFont)
		
		# ============================ Apple image parameters
		self.effect = StringVar(); self.effect.set("X EffectOff")
		self.effectOptionMenu = OptionMenu(rootFrame, self.effect, 
			"X EffectOff", "green", "orange", "violet", "blue", "white", "random", "even reverse",
			"inverse", "rnd noise", "abs(~byte)", "Mess image", "Zig Zag", "Zig Zag x2", "Zig Zag x3",
			"Zig Zag x4", "Zig Zag x5", "Zig Zag x6", "alt stripes",
			"v-lines inv", "v-lines rbow", "v-lines db inv", "rnd average", "dark v-lines", "free filter", command=self.refreshAppleImage)
		self.effectOptionMenu.configure(font=self.customFont)
		
		self.noise = Scale(rootFrame, from_=0, to=254, showvalue=0, label="Byte + 0-255", font=self.customFont, orient=HORIZONTAL)
		self.noise.set(0)

		self.bitWiseButton = Button(rootFrame, text=self.bitWise, font=self.customFont, command=self.openBitWise)
		
		self.randNoise = IntVar()
		self.randNoiseButton = Checkbutton(rootFrame, text="Random noise", font=self.customFont, variable=self.randNoise)

		self.warp = Scale(rootFrame, from_=0, to=100, showvalue=0, label="Warp", font=self.customFont, orient=HORIZONTAL)
		self.warp.set(0)
		
		# ============================ warp frame
		self.warpSoft = IntVar()
		self.warpSoftButton = Checkbutton(self.warpFrame, text="Soft warp", font=self.customFontMini, variable=self.warpSoft)
		
		self.zigZagWarp = IntVar()
		self.zigZagWarpButton = Checkbutton(self.warpFrame, text="Zigzag warp", font=self.customFontMini, variable=self.zigZagWarp)
		# ============================ end warp frame
		
		self.linesBetween = Scale(rootFrame, from_=0, to=100, showvalue=0, label="Add lines", font=self.customFont, orient=HORIZONTAL)
		self.linesBetween.set(0)

		self.linesByteVal = Scale(rootFrame, from_=0, to=255, showvalue=0, label="Byte val 0-255", font=self.customFont, orient=HORIZONTAL)
		self.linesByteVal.set(0)

		self.lineStep = Scale(rootFrame, from_=0, to=40, showvalue=0, label="Columns", font=self.customFont, orient=HORIZONTAL)
		self.lineStep.set(40)
		
		# ============================ apple screen frame
		self.labelAppleScreen = Label(self.appleScreenFrame, font=self.customFontBold, text="Apple Screen (Binary Image Result)")

		self.canAppleScreen = Canvas(self.appleScreenFrame, width=self.appleScr[0], height=self.appleScr[1], cursor="hand1", borderwidth=1, background="black")
		self.canAppleScreen.bind("<Button-1>",self.zoomImageResult)
		self.canAppleScreen.bind("<Button-2>",self.appleScreenImageBig)
		self.canAppleScreen.bind("<Button-3>",self.saveFullData)
		# ============================ end apple screen frame
		
		# ============================ MSB / Apple screen
		self.bit_MSB = IntVar()
		self.bit_MSB_Button = Checkbutton(rootFrame, text="MSB = 1", font=self.customFont, variable=self.bit_MSB)
		self.bit_MSB_Button.bind("<ButtonRelease-1>", lambda event:self.widgetDeselect(w=self.randomMsbButton))
		
		self.random_MSB = IntVar()
		self.randomMsbButton = Checkbutton(rootFrame, text="Rand MSB", font=self.customFont, variable=self.random_MSB)
		self.randomMsbButton.bind("<ButtonRelease-1>", lambda event:self.widgetDeselect(w=self.bit_MSB_Button))
		
		self.appleScreenGreen = IntVar()
		self.appleScreenGreenButton = Checkbutton(rootFrame, text="Green Scr", font=self.customFont, variable=self.appleScreenGreen)
		self.appleScreenGreenButton.bind("<ButtonRelease-1>", lambda event:self.widgetDeselect(w=self.appleScreenAmberButton))
		
		self.appleScreenAmber = IntVar()
		self.appleScreenAmberButton = Checkbutton(rootFrame, text="Amber Scr", font=self.customFont, variable=self.appleScreenAmber)
		self.appleScreenAmberButton.bind("<ButtonRelease-1>", lambda event:self.widgetDeselect(w=self.appleScreenGreenButton))
		
		# ============================ pixel grid
		self.pixelFrame = Frame(rootFrame, borderwidth=3, relief=GROOVE) # 2 rows X 4 cols
		
		labelPixelGrid = Label(self.pixelFrame,font=self.customFontBold, justify=LEFT, text="Only for Png result, click zoom to see (not for apple screen)")
		
		self.pixelGrid = IntVar()
		self.pixelGridButton = Checkbutton(self.pixelFrame, text="Pixel Grid", font=self.customFont, variable=self.pixelGrid)
		
		self.pixelGridColSize = Scale(self.pixelFrame, from_=0, to=100, showvalue=0, label="Row width", font=self.customFont, orient=HORIZONTAL)
		self.pixelGridRowSize = Scale(self.pixelFrame, from_=0, to=100, showvalue=0, label="Column width", font=self.customFont, orient=HORIZONTAL)
		
		self.pixelGridColorButton = Button(self.pixelFrame, text="Color", font=self.customFontBold, width=8, command=self.setPixelGridColor)
		
		# ============================ reset refresh
		self.refreshButton = Button(rootFrame, text="Refresh !", font=self.customFontBold, background="green3", activebackground="green1", width=8, command=self.refreshAppleImage)
		self.resetButton = Button(rootFrame, text="Reset !", font=self.customFontBold, background="green3", activebackground="green1", width=8, command=self.resetOriginal)
		
		# ============================ hide text
		labelImageMark = Label(rootFrame,font=self.customFontBold, justify=LEFT, text="Invisible text mark")
		
		self.imageMarkContent = StringVar()
		self.imageMark = Entry(rootFrame, font=self.customFont, width=81, textvariable=self.imageMarkContent)
		self.imageMark.bind("<Control-a>", self.selectImageMarkContent)
		
		# ============================ hexadecimal
		self.labelHex = Label(rootFrame, font=self.customFontBold, text="Hexadecimal Code ($2000 : $3FFF for call -151)")
		
		self.rawHex = IntVar()
		self.rawHexButton = Checkbutton(rootFrame, text="Raw hexadecimal code", font=self.customFontBold, variable=self.rawHex)
		self.labelRawHexFillColor = Label(rootFrame, font=self.customFont, text="Filling bytes color")
		
		self.rawHexFillColor = StringVar()
		self.rawHexFillColor.set("Black")
		self.rawHexFillOptions = OptionMenu(rootFrame, self.rawHexFillColor, 
			"Black", "White", "Violet", "Blue", "Green", "Orange", "Random", "Random X", "Random X2"
		)
		self.rawHexFillOptions.configure(font=self.customFont)
		self.rawHexFillColorValues =  {
			"Black":0, "White":255, "Violet":42, "Blue":170, "Green":85, 
			"Orange":213, "Random":0, "Random X":0, "Random X2":0
		}

		self.fieldCodeHexa = Text(rootFrame, font=self.customMonoFont, width=120, height=18, relief=SUNKEN, undo=True)
		self.fieldCodeHexa.bind("<Control-a>", self.selectFieldContent)
		self.fieldCodeHexa.bind("<Control-Up>", self.zoomTextIn)
		self.fieldCodeHexa.bind("<Control-Down>", self.zoomTextOut)
		self.fieldCodeHexa.bind("<Control-n>", self.newImageSource)
		self.fieldCodeHexa.bind("<ButtonRelease-1>", self.getSelection)
		self.fieldCodeHexa.bind("<Alt_L>", self.doRawHex)
		
		self.scrollbarHexa = Scrollbar(rootFrame)
		self.scrollbarHexa.configure(command=self.fieldCodeHexa.yview)
		self.fieldCodeHexa.config(yscrollcommand=self.scrollbarHexa.set)
	
		# ============================ bind widgets
		for widget in [self.brightness, self.contrast, self.shiftRight, self.shiftDown, self.warp, self.noise, self.linesBetween, self.linesByteVal, self.lineStep]:
			widget.bind("<ButtonRelease-1>", self.refreshAppleImage) # on release mouse refresh image
			
		for widget in [self.ditherButton, self.bit_MSB_Button, self.randomMsbButton, self.appleScreenGreenButton, self.appleScreenAmberButton, self.warpSoftButton, self.zigZagWarpButton, self.rawHexButton]:
			widget.bind("<Leave>", self.refreshAppleImage) # on leave button refresh image
		# ============================ end bind
		
		# ============================ grid
		rootFrameGrid = [	
			# grid representation r: row, c: column, rs: rowspan, cs: columnspan
			# widget							r  c  rs cs sticky		row
			# --------------------------------------------------------- 0
			[self.labelHelp, 					0, 0, 2, 1, N+W],
			[self.appleScreenFrame, 			0, 1, 7, 2, W+E+S],
			[self.geometryOptionMenu, 			0, 3, 1, 1, W+S],
			[self.effectOptionMenu, 			0, 4, 1, 1, W+S],
			[self.bitWiseButton, 				0, 5, 1, 1, W+S],
			[self.warp, 						0, 6, 1, 1, W+E+S],
			[self.warpFrame, 					0, 7, 1, 1, W+S],
			# --------------------------------------------------------- 1
			[self.shiftRight, 					1, 3, 1, 1, W+E+S],
			[self.noise, 						1, 4, 1, 1, W+E+S],
			[self.linesBetween, 				1, 5, 1, 1, W+E+S],
			[self.linesByteVal, 				1, 6, 1, 1, W+E+S],
			[self.lineStep, 					1, 7, 1, 1, W+E+S],
			# --------------------------------------------------------- 2
			[self.labelOriginal, 				2, 0, 1, 1, W+S],
			# --------------------------------------------------------- 3
			[self.canOriginal, 					3, 0, 4, 1, N+W],
			[self.shiftDown, 					3, 3, 4, 1, N+S],
			[self.refreshButton, 				3, 4, 1, 1, N+W+S],
			[self.resetButton, 					3, 5, 1, 1, N+W+S],
			[self.appleScreenGreenButton, 		3, 6, 1, 1, N+W],
			[self.appleScreenAmberButton, 		3, 7, 1, 1, N+W],
			# --------------------------------------------------------- 4
			[self.bit_MSB_Button, 				4, 6, 1, 1, N+W],
			[self.randomMsbButton, 				4, 7, 1, 1, N+W],
			# --------------------------------------------------------- 5
			[self.pixelFrame, 					5, 4, 2, 4, N+W+E],
			# --------------------------------------------------------- 6
			# --------------------------------------------------------- 7
			[self.labelPixelMap, 				7, 0, 1, 1, N+W],
			[labelImageMark, 					7, 1, 1, 1, N+W],
			[self.imageMark, 					7, 2, 1, 6, N+W],
			# --------------------------------------------------------- 8
			[self.canPixelMap, 					8, 0, 2, 1, N+W],
			[self.labelHex, 					8, 1, 1, 4, N+W+S],
			[self.rawHexButton, 				8, 4, 1, 2, N+W+S],
			[self.labelRawHexFillColor, 		8, 6, 1, 1, N+E+S],
			[self.rawHexFillOptions, 			8, 7, 1, 1, N+W],
			# --------------------------------------------------------- 9
			[self.fieldCodeHexa, 				9, 1, 1, 7, N+W+E+S],
			[self.scrollbarHexa, 				9, 8, 1, 1, N+S]
		]
		for w in rootFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
			
		warpFrameGrid = [
			# widget							r  c  rs cs sticky
			[self.warpSoftButton, 				0, 0, 1, 1, N+W],
			[self.zigZagWarpButton, 			1, 0, 1, 1, N+W],
		]
		for w in warpFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
			
		appleScreenFrameGrid = [
			# widget							r  c  rs cs sticky
			[imageTypeOptionMenu, 				0, 0, 1, 1, W+S],
			[self.ditherButton, 				0, 1, 1, 1, W+S],
			[self.brightness, 					1, 0, 1, 1, W+E+S],
			[self.contrast, 					1, 1, 1, 1, W+E+S],
			[self.labelAppleScreen, 			2, 0, 1, 2, N+W],
			[self.canAppleScreen, 				3, 0, 1, 2, N+W],
		]
		for w in appleScreenFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
			
		pixelFrameGrid = [
			# widget							r  c  rs cs sticky
			[labelPixelGrid, 					0, 0, 1, 4, W+S],
			[self.pixelGridButton, 				1, 0, 1, 1, N+W],
			[self.pixelGridColorButton,			1, 1, 1, 1, N+W],
			[self.pixelGridColSize, 			1, 2, 1, 1, N+W+E],
			[self.pixelGridRowSize, 			1, 3, 1, 1, N+W+E]
		]
		for w in pixelFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
		# ============================ end grid
		
		# ============================ configure
		widgetsConfig = appleScreenFrameGrid+pixelFrameGrid+[[self.appleScreenFrame],[self.pixelFrame],[self.canOriginal],[self.canPixelMap]]
		for w in widgetsConfig:
			w[0].configure(background=self.defaultBgAlt)
		# ============================ end configure
		
		# set resize to columns but not 1-2 where appleScreenImage is located
		for col in [0,3,4,5,6,7]:
			self.tkRoot.grid_columnconfigure(col, weight=1)
			
		self.tkRoot.grid_rowconfigure(9, weight=1)# resize row only for hexadecimal field

class Aie(AppleImageEditor):
	"""Class is Aie only a shortcut for AppleImageEditor"""
	def __init__(self):
		AppleImageEditor.__init__(self)
		
if __name__ == "__main__":
	print NAME,VERSION
	print ImgToAppleHGR.__doc__
	print ImgToAppleGR.__doc__
	print AppleImageEditor.__doc__
	print "\n".join([AUTHOR,COPYRIGHT,URL])
	o = AppleImageEditor()
	del o		
