Apple Image Editor
==================

Andres Lozano a.k.a Loz, 2013-2015
Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/

Python toolkit for image manipulation on "Apple II".
----------------------------------------------------

1. Creating and manipulating images, low and high resolution (GR and HGR)
2. Creating and manipulating "shapes"
3. Format data "bin" and "hexadecimal"

Apple Image Editor
------------------

Graphical user interface for convert and manipulate apple images

Apple tricks
------------
VTAB(22)  
HGR  
CALL -151 => open hex monitor  
* => paste hexadecimal code  
3D0G => hex code return to basic  
POKE -16302,0 => full screen  
POKE -16301,0 => mixed screen  
