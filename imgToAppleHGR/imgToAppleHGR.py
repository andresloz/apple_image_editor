#!/usr/bin/env python
#*- coding: utf-8 -*-

NAME = "Convert any image to Apple II High-Resolution (Hi-Res) graphics, HGR"
VERSION = "0.1.6 2015"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://virtualbasic.org"

from PIL import Image, ImageEnhance
from random import randint
from applePalette import loz_HGR_colors as apple_HGR_color

class ImgToAppleHGR:
	"""Class ImgToAppleHGR version 0.1.6 2015"""
	def __init__(self, 
			dither=1, bit_MSB=0, random_MSB=0, brightness=70, contrast=70,
			effect="", shiftRight=0, shiftDown=0, geometry="", noise=0, randNoise=0, 
			bitWise="^0000000", imageType=1, linesBetween=0, linesByteVal=0, 
			lineStep=1, warp=0, warpSoft=0, zigZagWarp=0
		):
		# Apple II HGR resolution 				280 * 192 	= 53760 pixels
		# Apple II HGR image bytes total		40  * 192 	= 7680  bytes
		# Apple II HGR total size (with screen holes)		= 8192  bytes
		# MSB bits 											= 7680 	bits
		# screen holes										= 512 	bytes
		self.Ascreen = (280, 192)				# Apple screen with/height
		# image instances
		self.imSrc = None						# image original instance
		self.imageSource = None					# image with type
		self.imageResult = None					# black white result only for copy here
		# bytes arrays
		self.appleBinData = []  				# array of image bytes in natural order
		self.appleBinImage = [0]*8192 			# array of image bytes in apple bin order
		self.appleBinColorMap = []				# HGR colors estimated map of image set to None
		self.pixelMap = [0]*53760				# pixel map = image in black/white
		# appleImage params
		self.showColorMap = 0					# boolean, only work if self.imSrc true
		self.mask = 0							# 0-255
		self.even = 1							# 0-~ even or odd
		self.hideText = "Apple Image Editor by Andres Lozano aka loz Copyleft 2015" # 512 bytes available for hide text
		# effects params
		self.hgrRGBcolors()						# init HRG in RGB values
		self.bit_MSB  = bit_MSB					# boolean
		self.random_MSB = random_MSB			# boolean
		self.dither = dither					# boolean
		self.brightness = brightness			# 0-100
		self.contrast = contrast				# 0-1OO
		self.effect = effect					# string
		self.shiftRight = shiftRight			# 0-40
		self.shiftDown = shiftDown				# 0-192		
		self.geometry = geometry				# string
		self.bitWise = bitWise					# string
		self.warp = warp 						# 0-279
		self.warpSoft = warpSoft				# boolean
		self.zigZagWarp = zigZagWarp			# boolean
		self.noise = noise						# 0-255
		self.randNoise = randNoise				# boolean
		self.imageType = imageType 				# 1-4
		self.linesBetween = linesBetween 		# 0-192
		self.linesByteVal = linesByteVal		# 0-255
		self.lineStep = lineStep				# 0-39
		
	def makeAppleImage(self):
		# image source => pixel map => appleBinData
		if self.imSrc: # if image source exist compute data from pict
			self.imageSource = self.setImageSource(im=self.imSrc) # create image source and set appleBinColorMap
			self.pixelMap = list(self.imageSource.getdata())
			self.appleBinData = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		else: 
			# if not self.imSrc ! you have to set previously : self.pixelMap, self.appleBinColorMap and self.appleBinData
			pass 
			
		# add post effects
		# appleBinData => pixel map => image result
		self.appleBinData = self.addEffects(bytes=self.appleBinData)
		self.pixelMap = self.byteToSevenPixels(bytes=self.appleBinData)
		self.imageResult = self.appleData2image(data=self.appleBinData)
		self.createAppleBin()
		
	def setImageSource(self, im=None):
		# resize image to apple screen sizes
		im = im.resize(self.Ascreen) 
		
		# set brightness
		enhancer = ImageEnhance.Brightness(im)
		factor = self.brightness * (2.0 / 100)
		if factor == 0.0: factor = 1 # zero => reset default val
		im = enhancer.enhance(factor)
		
		# set contrast
		contraster = ImageEnhance.Contrast(im)
		factor = self.contrast * (2.0 / 100)
		if factor == 0.0: factor = 1
		im = contraster.enhance(factor)

		# image conversion
		if self.imageType == "Black/White": 	im = im.convert("1", dither=self.dither) # pure black and white and color map none
		if self.imageType == "Adaptative": 		im = im.convert('RGB').convert('P', palette=Image.ADAPTIVE, colors=256) # use adapdative palette
		if self.imageType == "WEB palette": 	im = im.convert('RGB').convert('P', palette=Image.WEB, dither=self.dither) # use web palette 
		if self.imageType == "HGR adaptative": 	im = self.imageAdaptativeHgr(im=im) # special adapdative HGR 
		if self.imageType == "HGR": 			pass # default
		# get a color map from original image for all conversions
		self.createAppleBinColorMap(im=im)
		# convert to black and white for pixel map
		return im.convert("1", dither=self.dither)
		
	def imageAdaptativeHgr(self, im=None):
		# create a special adaptative image with HGR rgb colors
		applePalette = []
		applePalette.extend(apple_HGR_color["black"])
		applePalette.extend(apple_HGR_color["green"])
		applePalette.extend(apple_HGR_color["violet"])
		applePalette.extend(apple_HGR_color["white"])
		applePalette.extend(apple_HGR_color["orange"])
		applePalette.extend(apple_HGR_color["blue"])
		applePalette.extend([0,0,0]*250)
		applePaletteImage = Image.new("P", (1, 1), 0)
		applePaletteImage.putpalette(applePalette)
		# resize to 40 to have main color of each bytes and quantize to have apple colors
		im = im.convert("RGB").resize((280,192)).quantize(palette=applePaletteImage)
		return im
	
	def createAppleBinColorMap(self, im=None):
		# HGR rgb colors
		applePalette = []
		applePalette.extend(apple_HGR_color["black"])
		applePalette.extend(apple_HGR_color["green"])
		applePalette.extend(apple_HGR_color["violet"])
		applePalette.extend(apple_HGR_color["white"])
		applePalette.extend(apple_HGR_color["orange"])
		applePalette.extend(apple_HGR_color["blue"])
		applePalette.extend([0,0,0]*250)
		applePaletteImage = Image.new("P", (1, 1), 0)
		applePaletteImage.putpalette(applePalette)
		# resize to 40 to have main color of each bytes and quantize to have apple colors
		colorMap = im.convert("RGB").resize((40,192)).quantize(palette=applePaletteImage)
		# the color map = gedata() from quantize
		self.appleBinColorMap = list(colorMap.convert("RGB").getdata()) # 7680 elements
		return 1
		
	def getByteColor(self, byte=0, color=()):
		mask = 0; self.even += 1
		if color in [self.rgbGreen, self.rgbOrange]:
			mask = int("1010101",2)
		if color in [self.rgbViolet, self.rgbBlue]:
			mask = int("0101010",2)
			
		if self.even % 2 == 0: mask = ~mask
		
		# except Black/White
		if color == self.rgbGreen:
			byte = byte & mask	
		if color == self.rgbViolet:
			byte = byte & mask
		if color == self.rgbBlue:
			byte = byte & mask
			byte += 1<<7 # add MSB
		if color == self.rgbOrange:
			byte = byte & mask
			byte += 1<<7 # add MSB
			
		return byte
		
	def pixelsToAppleBytes(self, pixelMap=[]):
		if self.showColorMap:pixelMap = [255]*53760 # if only color map set pixel map to white
		byte = pos = k = 0
		bytes = []
		for e in pixelMap:
			if e == 255:byte += 1<<pos # inverse bit order : last right bit is first left pixel on screen
			pos +=1
			
			if pos == 7:
				if self.appleBinColorMap: # not create when image from binary without dat file
					color = self.appleBinColorMap[k];k += 1
					# parse each color and byte
					byte = self.getByteColor(byte=byte, color=color)
				
				bytes.append(byte)
				byte = pos = 0
			
		return bytes
		
	def byteToSevenPixels(self, bytes=[]):
		newBytes = []
		for byte in bytes:
			byteStr = str(bin(byte))[2:].zfill(8)
			byteStr = byteStr[1:] # pop Most significant bit
			byteStr = byteStr[::-1] # inverse bits order
			for i in xrange(0,7):
				if byteStr[i] == "1":
					newBytes.append(255) # use "L" image mode
				else:
					newBytes.append(0)
		return newBytes
		
	def appleData2image(self, data=[]): # black/white image of result
		stringData = []
		for byte in data:
			byteStr = str(bin(byte))[2:].zfill(8)
			byteStr = byteStr[::-1] # inverse bits order
			byteStr = byteStr[0:7] # select only first 7 bits
			for b in byteStr:
				if b == "1":
					stringData.append(chr(255))
				else:
					stringData.append(chr(0))
		
		return Image.frombytes("L", self.Ascreen, "".join(stringData))
			
	def createAppleBin(self): # re-order bytes in apple II system
		x = y = s_1 = s_2 = s_3 = 0
		for i in xrange(0,192): # 192 lines
			if i != 0: # avoid first 0
				if i % 8  == 0: s_1 = 0
				if i % 8  == 0: s_2 += 128
				if i % 64 == 0: s_2 = 0
				if i % 64 == 0: s_3 += 40
				
			x = s_1 + s_2 + s_3
			y = x + 40
			
			a = i*40; b = a+40; self.appleBinImage[x:y] = self.appleBinData[a:b]
			
			s_1 += 1024
			
		self.hideTextInImage()
		return 1
		
	def hideTextInImage(self):
		# hide text in the 512 free bytes of the image
		hideText = [ord(c) for c in self.hideText]
		k = 0
		for i in xrange(0,8192,128):
			for j in xrange(0,8):
				if k < len(hideText):
					self.appleBinImage[i+120+j] = hideText[k]
					k += 1
				else:
					self.appleBinImage[i+120+j] = 0
		return 1
		
	def getHexaCode(self): # hex code
		# convert dec values to hex
		result = ""
		startOffset = 8192 # $2000 hgr adress = 8192 dec
		for i in xrange(0,8192,32): # 32 to have equal lines
			tmp = [hex(x)[2:].upper().zfill(2) for x in self.appleBinImage[i:i+32]]
			# create line with address
			result += hex(startOffset)[2:].zfill(4).upper()+":"+" ".join(tmp)+"\n"
			startOffset += 32

		return result
	
	def byteOverFlow(self, bytes=[], where=""):
		# sometimes errors come in byte value and they must be fixed (ex: hexadecimal edition)
		if max(bytes) > 255: 
			print "Byte overflow here", where, ", value", max(bytes)
			newBytes = []
			for byte in bytes:
				if byte > 255:
					byte = 255
				newBytes.append(byte)
			bytes = newBytes
		return bytes
			
	# --------------------------------------------------------------------------------- effects
	def addEffects(self, bytes=[]):
		# effects
		bytes = self.byteOverFlow(bytes=bytes, where="add effects begin")
		bytes = self.addImageEffects(bytes=bytes)
		if self.linesBetween: bytes = self.addLines(bytes=bytes)
		bytes = self.addBitWise(bytes=bytes)
		bytes = map(self.addByteEffects, bytes) # byte by byte
		
		# add MSB only after special effects
		if self.bit_MSB: bytes = self.addBit_MSB(data=bytes)
		if self.random_MSB: bytes = self.addRandom_MSB(data=bytes)
		bytes = self.byteOverFlow(bytes=bytes, where="after random msb")
		return bytes
	
	def addBitWise(self, bytes=[]):
		if self.bitWise != "^0000000":
			for i in xrange(0, len(bytes)):
				bitEffect = str(bytes[i])+self.bitWise[0]+str(int(self.bitWise[1:],2))
				bytes[i] = eval(bitEffect)
				
		return bytes
	
	def addByteEffects(self, byte=0):
		self.even += 1
		
		if self.effect in ["random", "green", "orange", "violet", "blue"]:
			if self.effect == "random":
				self.mask = randint(0,127)
			elif self.effect in ["green", "orange"]:
				self.mask = int("1010101",2) # odd bits
			elif self.effect in ["violet", "blue"]:
				self.mask = int("0101010",2) # even bits
				
			mask = self.mask
			
			
			if byte > 127: byte = byte - 128 # pop MSB
			
			# colors
			if self.even % 2 == 0: mask = ~self.mask  # even column inverse mask
			# -----------------------------------------------------------------------------	MSB = 0		
			if self.effect == "green":
				byte = byte & mask	

			elif self.effect == "violet":
				byte = byte & mask
			#------------------------------------------------------------------------------	MSB = 1
			elif self.effect == "blue":
				byte = byte & mask
				byte += 1<<7 # add MSB
				
			elif self.effect == "orange":
				byte = byte & mask
				byte += 1<<7 # add MSB
			# -----------------------------------------------------------------------------	end MSB
			elif self.effect == "random":
				byte = byte & mask
				if byte < 128: byte += randint(0,1) * 128

		# filters
		elif self.effect in ["white","black"]: # true black/white on HIRES color screen
			byteStr = str(bin(byte)[2:]).zfill(7)
			byte = 0
			if byteStr[6] == "1":
				byte += int("11",2)
			if byteStr[4] == "1":
				byte += int("1100",2)
			if byteStr[3] == "1":
				byte += int("11110000",2)
			
		elif self.effect == "even reverse":
			if self.even % 2 == 0:
				byteStr = bin(byte)[2:].zfill(7)[::-1]
				byte = int("".join(byteStr),2)
				
		elif self.effect == "inverse":
			byte = byte ^ int("1111111",2)
			
		elif self.effect == "abs(~byte)":
			byte = abs(~byte)
			
		# ===================================
		elif self.effect == "v-lines inv":
			byte = byte ^ int("1111111",2)
			byte = byte | int("1001100",2)
			
		elif self.effect == "v-lines rbow":
			byte = abs(~byte)
			byte = byte ^ int("1011101",2)
			
		elif self.effect == "v-lines db inv":
			byte = abs(~byte)
			byte = byte | int("0010001",2)
			
		elif self.effect == "rnd average":
			mask = randint(100,200)
			byte = abs(~byte)
			byte = byte | mask
			
		elif self.effect == "alt stripes":
			mask = self.mask
			if self.even % 3 == 0:
				mask = ~self.mask
			byte = byte & mask
			
		elif self.effect == "dark v-lines":
			if byte > 127: byte = byte - 128 # pop MSB
			byteStr = bin(byte)[2:].zfill(7)[::-1]
			byte = 0; y = "999" # reset byte, first bit not effect, 
			for i in xrange(0,6):
				if y != '1':
					byte += int(byteStr[i])<<i	
				y = byteStr[i]
				
		elif self.effect == "free filter":
			# write your filter
			pass
		return byte
		
	def addImageEffects(self, bytes=[]):
		#affect pixelMap and sometimes colorMap
		if self.warp:
			if self.warpSoft:
				bytes = self.doWarpSoft()
			else:
				bytes = self.doWarp()
		if self.effect == "Zig Zag":
			bytes = self.doZigZagWarp(zig=1, freq=8)
		if self.effect == "Zig Zag x2":
			bytes = self.doZigZagWarp(zig=1, freq=24)
		if self.effect == "Zig Zag x3":
			bytes = self.doZigZagWarp(zig=1, freq=64)
		if self.effect == "Zig Zag x4" or self.zigZagWarp:
			bytes = self.doZigZagWarp(zig=1, freq=96)
		if self.effect == "Zig Zag x5":
			bytes = self.doZigZagWarp(zig=2, freq=96)
		if self.effect == "Zig Zag x6":
			bytes = self.doZigZagWarp(zig=2.5, freq=96)
		if self.effect == "Mess image":
			# mess image
			bytes = self.messImage()
		if self.geometry == "Flip":
			# flip bytes
			bytes = self.flipImage()
		if self.geometry == "UpsideDown":
			# upsideDown bytes
			bytes = self.upsideDownImage()
		if self.geometry == "Down+Flip":
			# upsideDown bytes
			bytes = self.bothUpsideDownFlip()
		if self.geometry == "Mirror H":
			# mirror Horizontal bytes
			bytes = self.mirrorHorizontal()
		if self.geometry == "Mirror H inv":
			# mirror Horizontal bytes
			bytes = self.mirrorHorizontal2()
		if self.geometry == "Mirror V":
			# mirror vertical bytes
			bytes = self.mirrorVertical()
		if self.geometry == "Mirror V inv":
			# mirror vertical bytes
			bytes = self.mirrorVertical2()
		if self.geometry == "Mirror H+V":
			# mirror vertical bytes
			bytes = self.bothMirrors()
			
		# bytes only not pixelMap
		if self.shiftRight:
			# shift right
			newArray = bytes[len(bytes)-self.shiftRight:]
			newArray.extend(bytes[0:len(bytes)-self.shiftRight])
			bytes = newArray
		if self.shiftDown:
			newArray = bytes[len(bytes)-(self.shiftDown*40):]
			newArray.extend(bytes[0:len(bytes)-(self.shiftDown*40)])
			bytes = newArray
		if self.noise:
			bytes = [abs(x - self.noise) for x in bytes]
		if self.effect == "rnd noise":
			if self.noise == 0:self.noise = randint(64,128) # not change MSB
			bytes = [abs(x - randint(0,self.noise)) for x in bytes]
			
		return bytes
		
	# image effect functions
	def messImage(self):
		newData = []
		while 1:
			a = randint(0,len(self.pixelMap)-1)
			b = randint(a,len(self.pixelMap))
			tmp = self.pixelMap[a:b]
			if randint(0,100) > 50:
				newData.extend(tmp)
			else:
				newData.extend(tmp[::-1])
				
			if len(newData) > len(self.pixelMap):
				newData = newData[0:len(self.pixelMap)]
				break
				
		self.pixelMap = newData
		
		if self.appleBinColorMap:
			newAppleBinColorMap = []
			while 1:
				a = randint(0,len(self.appleBinColorMap)-1)
				b = randint(a,len(self.appleBinColorMap))
				tmp = self.appleBinColorMap[a:b]
				if randint(0,100) > 50:
					newAppleBinColorMap.extend(tmp)
				else:
					newAppleBinColorMap.extend(tmp[::-1])
					
				if len(newAppleBinColorMap) > len(self.appleBinColorMap):
					self.appleBinColorMap = newAppleBinColorMap[0:len(self.appleBinColorMap)]
					break
		
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
			
	def bothUpsideDownFlip(self):
		self.pixelMap.reverse() # do reverse and flip
		
		if self.appleBinColorMap:self.appleBinColorMap.reverse()
		
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def bothMirrors(self):
		newData = []
		for i in xrange(0,len(self.pixelMap)/2,280):
			line = self.pixelMap[i:i+140]
			newData.extend(self.pixelMap[i:i+140])
			newData.extend(line[::-1])
		
		for i in reversed(xrange(0,len(newData),280)):
			newData.extend(newData[i:i+280])
			
		self.pixelMap = newData
		
		if self.appleBinColorMap:
			newAppleBinColorMap = []
			for i in xrange(0,len(self.appleBinColorMap)/2,40):
				line = self.appleBinColorMap[i:i+20]
				newAppleBinColorMap.extend(self.appleBinColorMap[i:i+20])
				newAppleBinColorMap.extend(line[::-1])
			
			for i in reversed(xrange(0,len(newAppleBinColorMap),40)):
				newAppleBinColorMap.extend(newAppleBinColorMap[i:i+40])
				
			self.appleBinColorMap = newAppleBinColorMap
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def mirrorHorizontal(self):
		newData = []
		for i in reversed(xrange(0,len(self.pixelMap)/2,280)):
			newData.extend(self.pixelMap[i:i+280])
			
		newData.extend(self.pixelMap[0:len(self.pixelMap)/2])
		self.pixelMap = newData	
		
		if self.appleBinColorMap:
			newAppleBinColorMap = []
			for i in reversed(xrange(0,len(self.appleBinColorMap)/2,40)):
				newAppleBinColorMap.extend(self.appleBinColorMap[i:i+40])
			
			newAppleBinColorMap.extend(self.appleBinColorMap[0:len(self.appleBinColorMap)/2])
			self.appleBinColorMap = newAppleBinColorMap
			
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def mirrorHorizontal2(self):
		newData = []
		for i in reversed(xrange(len(self.pixelMap)/2,len(self.pixelMap),280)):
			newData.extend(self.pixelMap[i:i+280])
			
		newData.extend(self.pixelMap[len(self.pixelMap)/2:len(self.pixelMap)])
		self.pixelMap = newData	
		
		if self.appleBinColorMap:
			newAppleBinColorMap = []
			for i in reversed(xrange(len(self.appleBinColorMap)/2,len(self.appleBinColorMap),40)):
				newAppleBinColorMap.extend(self.appleBinColorMap[i:i+40])
			
			newAppleBinColorMap.extend(self.appleBinColorMap[len(self.appleBinColorMap)/2:len(self.appleBinColorMap)])
			self.appleBinColorMap = newAppleBinColorMap
			
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def mirrorVertical(self):
		for i in xrange(0,len(self.pixelMap),280):
			line = self.pixelMap[i:i+140]
			self.pixelMap[i+140:i+280] = line[::-1]
			
		if self.appleBinColorMap:
			newAppleBinColorMap = []
			for i in reversed(xrange(0,len(self.appleBinColorMap),40)):
				line = self.appleBinColorMap[i:i+20]
				self.appleBinColorMap[i+20:i+40] = line[::-1]	
			
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def mirrorVertical2(self):
		for i in xrange(0,len(self.pixelMap),280):
			line = self.pixelMap[i+140:i+280]
			self.pixelMap[i:i+140] = line[::-1]
			
		if self.appleBinColorMap:
			newAppleBinColorMap = []
			for i in reversed(xrange(0,len(self.appleBinColorMap),40)):
				line = self.appleBinColorMap[i+20:i+40]
				self.appleBinColorMap[i:i+20] = line[::-1]	
			
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def upsideDownImage(self):
		newData = []
		for i in reversed(xrange(0,len(self.pixelMap),280)):
			newData.extend(self.pixelMap[i:i+280])
			
		self.pixelMap = newData	
		
		if self.appleBinColorMap:
			newAppleBinColorMap = []
			for i in reversed(xrange(0,len(self.appleBinColorMap),40)):
				newAppleBinColorMap.extend(self.appleBinColorMap[i:i+40])
			self.appleBinColorMap = newAppleBinColorMap
			
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def flipImage(self):
		for i in xrange(0,len(self.pixelMap),280):
			line = self.pixelMap[i:i+280]
			self.pixelMap[i:i+280] = line[::-1]
			
		if self.appleBinColorMap:
			newAppleBinColorMap = []
			for i in reversed(xrange(0,len(self.appleBinColorMap),40)):
				line = self.appleBinColorMap[i:i+40]
				self.appleBinColorMap[i:i+40] = line[::-1]	
			
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def doWarp(self):
		decal = 0
		for i in xrange(0,len(self.pixelMap),280):
			line = self.pixelMap[i:i+280]
			line = line[decal:]+line[0:decal]
			decal += self.warp
			if decal > 280:
				decal = 0
			self.pixelMap[i:i+280] = line
				
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def doWarpSoft(self):
		decal = 0
		self.warp = self.warp / 10
		if self.warp > 10:
			self.warp = 20 - self.warp
			for i in reversed(xrange(0,len(self.pixelMap),280)): # reverse progression
				line = self.pixelMap[i:i+280]
				line = line[decal:]+line[0:decal]
				decal += self.warp
				if decal > 280: # avoid overflow
					decal = 0
				self.pixelMap[i:i+280] = line
		else:
			for i in xrange(0,len(self.pixelMap),280):
				line = self.pixelMap[i:i+280]
				line = line[decal:]+line[0:decal]
				decal += self.warp
				if decal > 280:
					decal = 0
				self.pixelMap[i:i+280] = line
				
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def doZigZagWarp(self, zig=1, freq=64):
		decal = 0
		change = False
		for i in xrange(0,len(self.pixelMap),280):
			if i % freq == 0:
				if change: 
					change = False
				else:
					change = True

			if change:		
				decal += zig
			else:
				decal -= zig
			
			if decal > 280 or decal < 0:
				decal = 0
				
			line = self.pixelMap[i:i+280]
			line = line[int(decal):]+line[0:int(decal)]
			self.pixelMap[i:i+280] = line
			
		bytes = self.pixelsToAppleBytes(pixelMap=self.pixelMap)
		return bytes
		
	def addLines(self, bytes=[]):
		lineNum = 0
		for i in xrange(0,(192*40),40):
			if lineNum % self.linesBetween == 0:
				if self.lineStep == 0: self.lineStep = 1
				for j in xrange(0,40,self.lineStep):
					bytes[i+j] = self.linesByteVal
			lineNum += 1
		return bytes
		
	def addBit_MSB(self, data=[]):
		bytes = []
		for byte in data:
			if byte < 128: byte += 128
			bytes.append(byte)
			
		return bytes
	
	def addRandom_MSB(self, data=[]):
		bytes = []
		for byte in data:
			if randint(0,1000) > 500:
				if byte < 128: # add MSB
					byte += 128
					bytes.append(byte)
				elif byte > 127: # remove MSB
					# if byte > 127 then MSB = 1
					byte -= 128
					bytes.append(byte)
			else:
				bytes.append(byte)
				
		return bytes
		
	# others
	def hgrRGBcolors(self):
		# self.rgbGreen = (51,204,0)
		# self.rgbViolet = (204,51,255)
		# self.rgbBlue = (0,153,255)
		# self.rgbOrange = (255,102,0)
		# self.rgbBlack = (0,0,0)
		# self.rgbWhite = (255,255,255)
		
		self.rgbGreen = apple_HGR_color["green"]
		self.rgbViolet = apple_HGR_color["violet"]
		self.rgbBlue = apple_HGR_color["blue"]
		self.rgbOrange = apple_HGR_color["orange"]
		self.rgbBlack = apple_HGR_color["black"]
		self.rgbWhite = apple_HGR_color["white"]
		return 1
		
	def saveImg(self, im=None, fileName="test.png", r=(0,0)):
		if r != (0,0):
			im = im.resize(r)
		try:
			im.save(fileName,"PNG")
			return 1
		except:
			print "can't save",fileName
			return 0
			
if __name__ == "__main__":
	print NAME
	print ImgToAppleHGR.__doc__
	print "\n".join([AUTHOR,COPYRIGHT,URL])
