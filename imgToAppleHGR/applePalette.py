#!/usr/bin/env python
#*- coding: utf-8 -*-

# loz GR colors
loz_GR_colors = {
 	"black":		(0,		0,		0),		# 0
	"red":			(153,	0,		102),	# 1
	"dark-blue":	(51,	51,		204),	# 2
	"purple":		(204,	51,		255),	# 3
	"dark-green":	(0,		102,	51),	# 4
	"gray":			(153,	153,	153),	# 5
	"medium-blue":	(0,		153,	255),	# 6
	"light-blue":	(153,	153,	255),	# 7
	"brown":		(102,	102,	0),		# 8
	"orange":		(255,	102,	0),		# 9
	"grey":			(204,	204,	204),	# 10
	"pink":			(255,	153,	204),	# 11
	"light-green":	(51,	204,	0),		# 12
	"yellow":		(204,	204,	51),	# 13
	"aqua":			(102,	255,	153),	# 14
	"white":		(255,	255,	255)	# 15
}
# mrob GR colors
mrob_GR_colors = {
	"black":		(0,		0,		0), 	# 0
	"red":			(227,	30,		96),	# 1
	"dark-blue":	(96,	78,		189),	# 2
	"purple":		(255,	68,		253),	# 3
	"dark-green":	(0,		163,	96),	# 4
	"gray":			(156,	156,	156),	# 5
	"medium-blue":	(20,	207,	253),	# 6
	"light-blue":	(208,	195,	255),	# 7
	"brown":		(96,	114,	3),		# 8
	"orange":		(255,	106,	60),	# 9
	"grey":			(156,	156,	156),	# 10
	"pink":			(255,	160,	208),	# 11
	"light-green":	(20,	245,	60),	# 12
	"yellow":		(208,	221,	141),	# 13
	"aqua":			(114,	255,	208),	# 14
	"white":		(255,	255,	255) 	# 15
}
 
# loz HGR colors
loz_HGR_colors = {
	"black":		(0,		0,		0),		# 0
	"green":		(51,	204,	0),		# 1
	"violet":		(204,	51,		255),	# 2
	"white":		(255,	255,	255),	# 3
	# "black":		(0,		0,		0),		# 4
	"orange":		(255,	102,	0),		# 5
	"blue":			(0,		153,	255),	# 6
	# "white":		(255,	255,	255)	# 7	
}
# mrob HGR colors
mrob_HGR_colors = {
	"black":		(0,		0,		0), 	# 0
	"green":		(20,	245,	60),	# 1
	"violet":		(255,	68,		253),	# 2
	"white":		(255,	255,	255),	# 3
	# "black":		(0,		0,		0),		# 4
	"orange":		(255,	106,	60),	# 5
	"blue":			(20,	207,	253),	# 6
	# "white":		(255,	255,	255)	# 7
}
 

# mrob colors 
# black    COLOR=0    0   0    0      0   0   0
# red      COLOR=1   90  60   25    227  30  96
# dk blue  COLOR=2    0  60   25     96  78 189
# purple   COLOR=3   45 100   50    255  68 253
# dk green COLOR=4  270  60   25      0 163  96
# gray     COLOR=5    0   0   50    156 156 156
# med blue COLOR=6  315 100   50     20 207 253 
# lt blue  COLOR=7    0  60   75    208 195 255 
# brown    COLOR=8  180  60   25     96 114   3 
# orange   COLOR=9  135 100   50    255 106  60 
# grey     COLOR=10   0   0   50    156 156 156 
# pink     COLOR=11  90  60   75    255 160 208
# lt green COLOR=12 225 100   50     20 245  60 
# yellow   COLOR=13 180  60   75    208 221 141 
# aqua     COLOR=14 270  60   75    114 255 208 
# white    COLOR=15   0   0  100    255 255 255

# green    HCOLOR=1 225 100   50     20 245  60
# purple   HCOLOR=2  45 100   50    255  68 253
# orange   HCOLOR=5 135 100   50    255 106  60
# blue     HCOLOR=6 315 100   50     20 207 253