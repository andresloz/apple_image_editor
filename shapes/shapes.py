#! /usr/bin/env python
r"""Shapes version 0.0.4
	Copyright 2014, andres lozano aka loz
	Copyleft: This work is free, you can redistribute it and / or modify it
	under the terms of the Free Art License. You can find a copy of this license
	on the site Copyleft Attitude well as http://artlibre.org/licence/lal/en on other sites.
"""

from random import choice, randint

class Shapes:
	r"""class Shapes version 0.0.4
	vectors = string of chars : l,r,u,d
	rightFlip(vectors)
	downFlip(vectors)
	rightDownFlip(vectors)
	diagonalSym(vectors)
	verticalSym(vectors)
	pattern(vectors)
	circular(vectors)
	circularInverse(vectors)
	closeShape(vectors)
	randomVectors(n = vectors amount)
	randomShapes(x = shape amount,n = vectors amount, t = type of shape: type of shape : default pattern, circular, circularInverse or mixed)
	"""
	
	def __init__(self):
		self.vectValues = {"l":(1,0),"r":(-1,0),"u":(0,1),"d":(0,-1)}
		# can't use noplot vectors because can't prevent two noplot consecutive witch is not allowed
		self.vectorPlotChars = ("l","r","u","d")

	def rightFlip(self,s):
		flip = []
		list = [x for x in s]
		for x in list:
			if x in ["l","r"]:
				flip.append(self.invVectorLRUD(x))
			elif x in ["u","d"]:
				flip.append(x)
				
		return flip
		
	def downFlip(self,s):
		flip = []
		list = [x for x in s]
		for x in list:
			if x in ["l","r"]:
				flip.append(self.invVectorLRUD(x))
			elif x in ["u","d"]:
				flip.append(x)

		flip = flip[::-1]
		return flip
		
	def rightDownFlip(self,s):
		flip = []
		list = [x for x in s]
		for x in list:
			if x in self.vectorPlotChars:
				flip.append(self.invVectorLRUD(x))
				
		return flip
		
	def diagonalSym(self,s):
		list = [x for x in s]
		list.extend(self.rightDownFlip(s))
		return list
		
	def rotateOneStep(self,s):
		flip = []
		list = [x for x in s]
		for x in list:
			if x in self.vectorPlotChars:
				flip.append(self.invVectorLURDURDL(x))

		list.extend(flip)
		return list
		
	def circular(self,s):
		list = self.diagonalSym((self.rotateOneStep(s)))
		return list
		
	def circularInverse(self,s):
		s = self.rightFlip(s)[::-1]
		list = self.diagonalSym((self.rotateOneStep(s)))
		return list
		
	def horizontalSym(self,s):
		list = [x for x in s]
		list.extend(self.rightFlip(s))
		return list
		
	def verticalSym(self,s):
		list = [x for x in s]
		list.extend(self.downFlip(s))
		return list
		
	def pattern(self,s):
		list = self.horizontalSym(s)
		list = self.verticalSym(list)
		return list
		
	def invVectorLRUD(self,v):
		vector = {'l':'r','u':'d','r':'l','d':'u'}
		return vector[v]
		
	# def invVectorLURD(self,v):
		# vector = {'l':'u','r':'d','u':'l','d':'r'}
		# return vector[v]
		
	def invVectorLURDURDL(self,v):
		vector = {'l':'u','r':'d','u':'r','d':'l'}
		return vector[v]
		
	def closeShape(self,s):
		v = [0,0]
		for vector in s:
			if vector in self.vectorPlotChars:
				# add vectors values
				v = [x + y for x, y in zip(v, self.vectValues[vector])]
			
		# close shape
		if v[0] > 0:
			s += "r"*v[0]
		elif v[0] < 0:
			s += "l"*abs(v[0])
			
		if v[1] > 0:
			s += "d"*v[1]
		elif v[1] < 0:
			s += "u"*abs(v[1])
			
		return s
		
	def specialRand(self,vectors):
		newArray = []
		for vector in vectors:
			for i in range(1,randint(0,100)):
				newArray.append(vector)
		return newArray
		
	def randomVectors(self,n,t=1):
		if t:
			vectors = self.vectorPlotChars
		else:
			vectors = self.specialRand(self.vectorPlotChars)
		
		s = ""
		for i in range(n):
			vector = choice(vectors)
			s += vector
		
		# we need to close the shape and add new vectors for symmetric transformations
		# the size of the shape will grow a few in relation with the shape form
		s = self.closeShape(s)	
		return s
	
	def randomShapes(self,n,x,t="pattern"):
		shapes = []
		for i in range(n):
			s = ""
			if t == "mixed":
				type = choice(["pattern","circular","circularInverse"])
			else:
				type = t
				
			s = self.randomVectors(x)
				
			if type == "pattern":
				shape = self.pattern(s)
				shapes.append(shape)
			elif type == "circular":
				shape = self.circular(s)
				shapes.append(shape)
			elif type == "circularInverse":
				shape = self.circularInverse(s)
				shapes.append(shape)
				
		return shapes

class ShapesTable:
	r"""class ShapeTable version 0.0.4
	u : "plot up"
	d : "plot down"
	l : "plot left"
	r : "plot right"
	
	only one "no plot" at once
	t : "no plot up"
	c : "no plot down"
	k : "no plot left"
	q : "no plot right"
	
	shape table is generaly locate at $300 (768)
	DOS 3.3 "bload shapetable.bin" put table in $300
	you can put less than 255 bytes in $300 - $3FF
	if your table is bigger than 255 you can put it at $1D00 - $1FFF (768 bytes)
	with DOS 3.3 you have to set HIMEM:7424 ($1D00) to bload table at this offset
	(himem value = peek(115)+peek(116)*256)
	"""
	
	def __init__(self,shapesVectors=[]):
		self.vectorsAmount = 0
		self.shapesTableData = []
		self.shapesValues = self.makeShapesTable(shapesVectors)
	
	def makeShapesTable(self,shapesVectors):
		allShapesIntArray = []
		tableIndex = {}
		shapeNumOrder = 0
		for shapeVectors in shapesVectors:
			shapeIntArray = self.vectors2Ints(shapeVectors)
			shapeNumOrder += 1
			tableIndex.update({shapeNumOrder:len(shapeIntArray)})
			allShapesIntArray.extend(shapeIntArray)
			
		# table index creation
		nextShapePosIndex = 0
		shapeIndex = []
		shapeIndex.append(len(tableIndex)) # total of shapes in first cell
		shapeIndex.append(0) # unused cell
		
		firstShapePosIndex = (len(tableIndex) * 2) + 2 #  total index shapes cells
		for shapeNumOrder,length in tableIndex.items():
			if shapeNumOrder == 1:
				if firstShapePosIndex > 255: # need to convert values into two bytes (two cells) for position
					shapeIndex.extend(self.twoBytes(firstShapePosIndex))
				else:
					shapeIndex.extend([firstShapePosIndex,0])
					
				nextShapePosIndex = firstShapePosIndex+length
			else:
				if nextShapePosIndex > 255: # need to convert values into two bytes (two cells) for position
					shapeIndex.extend(self.twoBytes(nextShapePosIndex))
				else:
					shapeIndex.extend([nextShapePosIndex,0])
					
				nextShapePosIndex = nextShapePosIndex+length
		
		# add shapes data 
		self.shapesTableData = shapeIndex+allShapesIntArray
		return len(self.shapesTableData),len(tableIndex),self.vectorsAmount
	
	def twoBytes(self,x):
		lowByte = x % 256
		highByte = x / 256
		return lowByte,highByte
		
	def vectors2Ints(self,vectors):
		# bit vectors values
		plot = "1"
		noplot = "0"
		# moves
		up = "00"
		down = "10"
		left = "11" 
		right = "01"
		
		plotVal = {
			"u":plot+up, 		# "100"
			"d":plot+down, 		# "110"
			"l":plot+left,		# "111"
			"r":plot+right,		# "101"
			"t":noplot+up,		# "000"
			"c":noplot+down,	# "010"
			"k":noplot+left,	# "011"
			"q":noplot+right	# "001"
			}
					
		tmpByte = "";intArray = []
		for vector in vectors:
			# don't add separators, only vectors
			if vector in plotVal.keys():
				self.vectorsAmount += 1
				tmpByte = plotVal[vector]+tmpByte
				
			if len(tmpByte) == 6:
				# finish shape
				tmpByte = "00"+tmpByte
				intArray.append(int(tmpByte,2))
				tmpByte = ""
		
		# add 0 to separe shapes
		intArray.append(0)
		return intArray
		
	def memoryOffset(self):
		lenShapeTable,shapesAmount,vectorsAmount = self.shapesValues
		if lenShapeTable <= 255:
			# put it in $300 > dec = 768 > two bytes = 0 3
			offsetDec = 768
			offsetBytes = 0,3
		elif lenShapeTable <= 768 :
			# if table don't fit in $300
			# put it in $1D00 > dec = 7424 > two bytes = 0 29
			offsetDec = 7424
			offsetBytes = 0,29
		else:
			# or put it in second hires page at $4000 > dec = 16384 > two bytes = 0 64
			# offsetDec = "16384"
			# offsetBytes = "0","64"
			# or put it after second hires page at $6000 > dec = 24756 > two bytes = 0 96
			offsetDec = 24576
			offsetBytes = 0,96
		
		return offsetDec,offsetBytes
			
	def getDataBasicCode(self):
		# return shapes table like "DATA 65,70,..."
		lenShapeTable,shapesAmount,vectorsAmount = self.shapesValues
		export = []
		i = 0;data = [];maxData = 40;shapeNumber = 0
		lengthIndex = (shapesAmount * 2) + 2
		for x in self.shapesTableData:
			data.append(str(x))
			# cut shape at byte 0 after table index
			if i > lengthIndex and x == 0:
				shapeNumber += 1
				export.append( "rem shape num "+str(shapeNumber)+", bytes "+str(len(data)) )
				if len(data) > maxData:
					for z in range(len(data)/maxData):
						x1 = z * maxData
						x2 = x1 + maxData
						export.append("data "+",".join(data[x1:x2]))
						
					if len(data) >= z * maxData:
						export.append("data "+",".join(data[z*maxData+maxData:]))
							
					data = []
				else:
					export.append("data "+",".join(data))
					data = []
					
			# cut index data index at the end
			elif i == (shapesAmount * 2) + 1:
				export.append("rem table index")
				if len(data) > maxData:
					for z in range(len(data)/maxData):
						x1 = z * maxData
						x2 = x1 + maxData
						export.append("data "+",".join(data[x1:x2]))
						
					if len(data) >= z * maxData:
						export.append("data "+",".join(data[z*maxData+maxData:]))
							
					data = []
				else:
					export.append("data "+",".join(data))
					data = []
			i += 1
		
		return export
		
	
if __name__ == "__main__":		
	print "You are in shape class !"















