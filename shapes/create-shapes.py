  #! /usr/bin/env python
from shapes import ShapesTable, Shapes

# *****************************************************************************
# create yours shapes here

# vectors symbols
# u : "plot up"
# d : "plot down"
# l : "plot left" 
# r : "plot right"

# only one "no plot" at once
# t : "no plot up"
# c : "no plot down"
# k : "no plot left"
# q : "no plot right"

# 1) example shapes writed by hand
# shapes = []
# arrow = "llluldl uulu uluu luur rdrr drrd rrdl drddd"
# shapes.append(arrow)

# 2) example of shapes using class Shapes for easy manipulations and random creations
shapes = []
sh = Shapes()
for i in range(2):
	example = sh.randomVectors(32,0)
	shapes.append( example )
	shapes.append( sh.horizontalSym(example) )
	shapes.append( sh.verticalSym(example) )
	shapes.append( sh.pattern(example) )
	shapes.append( sh.circular(example) )
	shapes.append( sh.circularInverse(example) )
del sh

# 3) random shapes exemple
# shapes = []
# sh = Shapes()
# shapes = sh.randomShapes(number of shapes, base number of vector, type of shape : default t="pattern" or t="circular", t="circularInverse" and t="mixed" for random type)
# shapes.extend(sh.randomShapes(10,48,t="mixed")) 
# del sh
# *****************************************************************************








# -------------------------  no changes needed below -------------------------#
# *****************************************************************************
# auto generated code using class ShapesTable

# shape table creation
# object = class shapesTable(shapes = array of arrays)
shTable = ShapesTable(shapes)
# return three values : table data (all shapes), the amount of shapes and the amount of vectors
lenShapeTable,shapesAmount,vectorsAmount = shTable.shapesValues
shapesTableData = shTable.shapesTableData
dataBasicCode = shTable.getDataBasicCode() # get data in basic style
offsetDec, offsetBytes = shTable.memoryOffset() # get memory offset where put the table
del shTable


# basic code creation
codeData = []
codeData.append("poke 232,"+str(offsetBytes[0])+": poke 233,"+str(offsetBytes[1])+" : rem set table offset in memory")
codeData.append("rem "+str(shapesAmount)+" shapes, "+str(vectorsAmount)+" vectors, size "+str(lenShapeTable)+" bytes")
codeData.append("rem print chr$(4)\"bload shapetable.bin,a$"+hex(offsetDec)[2:].upper()+",l$"+hex(lenShapeTable)[2:].upper()+"\"")
codeData.append("rem or read data and put it in memory")
codeData.append("for i=0 to "+str(lenShapeTable-1))
codeData.append("read b")
codeData.append("poke "+str(offsetDec)+"+i,b")
codeData.append("next")
codeData.append("hgr : rem init hires screen page 1 and shape parameters")
codeData.append("hcolor = 3 : scale = 3 : rot = 0")
codeData.append("shapes = "+str(shapesAmount))
codeData.append("for i=1 to shapes : rem show all shapes")
codeData.append("call -3086 : rem reset screen to black between to shapes")
codeData.append("x2 = 0:y2 = 0 : rem reset recorded joystick position")
codeData.append("home:vtab(21) : print \"shape num \";i : rem show shape number")
codeData.append("for j=1 to 100 : rem temporize")
codeData.append("x1 = pdl(0) * 1.094 : rem pdl(x=0-255) read x joystick value")
codeData.append("y1 = pdl(1) * 0.623 : rem pdl(y=0-255) read y joystick value")
codeData.append("rem erase previous shape only if shapes position change")
codeData.append("rot=r2")
codeData.append("if x1 <> x2 or y1 <> y2 then hcolor = 0")
codeData.append("if x1 <> x2 or y1 <> y2 then draw i at x2,y2")
codeData.append("rem draw new shape only if shapes position change")
codeData.append("r1=j*2.5:rot=r1")
codeData.append("if x1 <> x2 or y1 <> y2 then hcolor = 3")
codeData.append("if x1 <> x2 or y1 <> y2 then draw i at x1,y1")
codeData.append("x2 = x1:y2 = y1:r2=r1: rem record position and angle")
codeData.append("next:next")
codeData.append("get a$")
codeData.append("text:home")
codeData.append("end")
codeData.append("rem "+str(shapesAmount)+" shapes, "+str(vectorsAmount)+" vectors, size "+str(lenShapeTable)+" bytes")
codeData.extend(dataBasicCode)
codeData.append("rem print chr$(4)\"bsave shapetable.bin,a$"+hex(offsetDec)[2:].upper()+",l$"+hex(lenShapeTable)[2:].upper()+"\"")

# add line numbers
finalCode = []
numLine = 0
for item in codeData:
	numLine += 10
	finalCode.append(str(numLine)+" "+item.upper())

# final basic code
finalCode = "NEW\n"+"\n".join(finalCode)+"\nRUN"

# show basic code in console
print finalCode

# save basic code into file
file = "example-basic.bas"
f = open(file,"w")
f.write(finalCode)
f.close()

# save shape table into binary file
onlyBytes = bytearray(shapesTableData)
file = "example-shapetable.bin"
f = open(file,"wb")
f.write(onlyBytes)
f.close()

# check data
if 0:
	# show shape table in dec and hex and bin
	hexArray = [hex(x)[2:].upper() for x in shapesTableData]
	binArray = [bin(x)[2:].zfill(8) for x in shapesTableData] 
	print 
	print str(shapesAmount)+" shapes, "+str(vectorsAmount)+" vectors, size "+str(lenShapeTable)+" bytes, hex:"
	print " ".join(hexArray)
	print
	print str(shapesAmount)+" shapes, "+str(vectorsAmount)+" vectors, size "+str(lenShapeTable)+" bytes, dec:"
	print " ".join([str(x) for x in shapesTableData])
	print
	print str(shapesAmount)+" shapes, "+str(vectorsAmount)+" vectors, size "+str(lenShapeTable)+" bytes, bin:"
	print " ".join(binArray)


# *****************************************************************************